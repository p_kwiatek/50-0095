
$(document).ready(function () {
    
    var isMobileView = false;
    var isLargeView = false;
    var isMediumView = false;
    var isModuleHeightAlign = false;

    $('.protectedPage').each(function(index) {
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="lock-object" viewBox="0 0 9 16"><defs><filter filterUnits="userSpaceOnUse" id="lock-filter-' + index + '" x="0px" y="0px" width="9px" height="16px"><feOffset in="SourceAlpha" dx="0.209" dy="1.989" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(1, 1, 1)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="0.16"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#lock-filter-' + index + ')"><path fill-rule="evenodd" d="M4.508,13.982 C2.034,13.982 0.028,11.976 0.028,9.502 C0.028,7.028 2.034,5.023 4.508,5.023 C4.654,5.023 4.798,5.030 4.941,5.044 C5.045,5.054 5.147,5.072 5.250,5.089 C5.283,5.095 5.317,5.098 5.350,5.104 C5.480,5.129 5.607,5.161 5.732,5.196 C5.735,5.197 5.738,5.198 5.741,5.198 C5.871,5.235 5.998,5.279 6.123,5.327 C6.123,5.327 6.123,5.327 6.123,5.328 L6.123,2.952 C6.123,2.033 5.293,1.285 4.272,1.285 C3.251,1.285 2.421,2.033 2.421,2.952 C2.421,3.232 2.194,3.459 1.914,3.459 C1.635,3.459 1.408,3.232 1.408,2.952 C1.408,1.475 2.693,0.272 4.272,0.272 C5.851,0.272 7.136,1.475 7.136,2.952 L7.136,5.698 C7.136,5.755 7.125,5.808 7.108,5.859 C7.109,5.859 7.110,5.860 7.111,5.861 C7.241,5.954 7.365,6.054 7.484,6.161 C7.505,6.179 7.526,6.198 7.547,6.218 C7.643,6.307 7.736,6.401 7.824,6.498 C7.847,6.523 7.870,6.548 7.892,6.573 C7.939,6.627 7.984,6.682 8.028,6.738 C8.064,6.783 8.097,6.829 8.130,6.875 C8.185,6.949 8.237,7.025 8.287,7.104 C8.323,7.160 8.357,7.217 8.391,7.275 C8.417,7.321 8.444,7.366 8.469,7.412 C8.519,7.507 8.565,7.604 8.609,7.703 C8.626,7.743 8.643,7.784 8.660,7.825 C8.704,7.933 8.745,8.043 8.780,8.155 C8.787,8.176 8.792,8.198 8.798,8.219 C8.831,8.326 8.858,8.435 8.882,8.545 C8.889,8.574 8.896,8.602 8.901,8.631 C8.926,8.756 8.945,8.882 8.959,9.010 C8.962,9.037 8.964,9.065 8.967,9.092 C8.979,9.228 8.987,9.364 8.987,9.502 C8.987,11.976 6.982,13.982 4.508,13.982 ZM5.215,8.559 C5.215,8.170 4.897,7.852 4.508,7.852 C4.119,7.852 3.800,8.170 3.800,8.559 L3.800,9.974 C3.800,10.363 4.119,10.681 4.508,10.681 C4.897,10.681 5.215,10.363 5.215,9.974 L5.215,8.559 Z"/></g></svg>').appendTo(this);
    });

    
    $(document).on("click", ".caption_nav_prev a", function ()
    {
        var date = $(this).attr('id').substr(2);
        $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

        $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
    });

    $(document).on("click", ".caption_nav_next a", function ()
    {
        var date = $(this).attr('id').substr(2);
        $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

        $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
    });

    $('#mod_calendar2 .module-content').each(function() {
        $(this).prepend('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="calendar-arrow right" width="11px" height="18px"><defs><filter filterUnits="userSpaceOnUse" id="caption-nav-next-filter" x="0px" y="0px" width="11px" height="18px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(32, 61, 76)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#caption-nav-next-filter)"><path fill-rule="evenodd" d="M-0.005,16.244 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.244 L-0.005,16.244 Z"/></g></svg>');
        $(this).prepend('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="calendar-arrow" width="11px" height="18px"><defs><filter filterUnits="userSpaceOnUse" id="caption-nav-prev-filter" x="0px" y="0px" width="11px" height="18px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(32, 61, 76)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#caption-nav-prev-filter)"><path fill-rule="evenodd" d="M-0.005,16.244 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.244 L-0.005,16.244 Z"/></g></svg>');
    });

    $('.footer__gotop').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        $('.page').focus();
        return false; 
    });
    
    $('#btnFilePos').on('click', function() {
        $('#avatar_f').trigger('click');
    });

    if ($('.modules-top .module').length) {
        $('.modules-top2').addClass('padding-top');
    }

    if (!$('#navbar-top > ul').length) {
        $('#navbar-top .vector-decoration').hide();
        $('.header').addClass('no-menu');
    }

    if (!$('.footer__list > ul').length) {
        $('.footer__list .vector-decoration').hide();
    }

    $('.qBar').each(function() {
        if ($(this).width() <= 2) {
            $(this).hide();
        }
    });

    $('.menu-top .dropdown-submenu a.selected').closest('.dropdown-submenu').children('a').addClass('selected');

    if (isMobile.any) {
        $('body').addClass('is-mobile');

        $('.dropdown-submenu', '#navbar-top').on('hide.bs.dropdown', function () {
            return false;
        });
    }

    $(".board table, .main-text table").each(function() {
        var $table = $(this);
        
        if ($table.parent('.table-responsive').length === 0) {
            $table.wrap('<div class="table-responsive"></div>');
        }
    });

    $(".owl-carousel").owlCarousel({
        singleItem: true,
        autoPlay: 1e3 * settings.duration,
        slideSpeed: 1e3 * settings.animationDuration,
        paginationSpeed: 1e3 * settings.animationDuration,
        transitionStyle: settings.transition,
        pagination: false,
        mouseDrag: false,
        touchDrag: false
    });

    function equalizeHeights(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).outerHeight());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function fancyboxInit() {
        var a;
        $("a.fancybox").fancybox({
            overlayOpacity: .9,
            overlayColor: settings.overlayColor,
            titlePosition: "outside",
            titleFromAlt: !0,
            titleFormat: function (a, b, c, d) {
                return '<span id="fancybox-title-over">' + texts.image + " " + (c + 1) + " / " + b.length + "</span>" + (a.length ? " &nbsp; " + a.replace(texts.enlargeImage + ": ", "") : "")
            },
            onStart: function (b, c, d) {
                a = b[c];
            },
            onComplete: function () {
            },
            onClosed: function () {
            }
        });
    }

    fancyboxInit();

    if (popup.show)
    {
        $.fancybox(
                popup.content,
                {
                    overlayOpacity: .9,
                    overlayColor: settings.overlayColor,
                    padding: 20,
                    autoDimensions: !1,
                    width: popup.width,
                    height: popup.height,
                    transitionIn: "fade",
                    transitionOut: "fade",
                    onStart: function (a) {
                        $("#fancybox-outer").css({
                            background: popup.popupBackground
                        });
                        $("#fancybox-content").addClass("main-text no-margin");
                    }
                }
        );
    }

    $('.toolbar__links .search').on('click', 'button', function (e) {
        if (Modernizr.mq('(max-width: 519px)')) {
            e.stopPropagation();

            if (!$(e.delegateTarget).hasClass('show')) {
                e.preventDefault();
                $(e.delegateTarget).addClass('show');
                $(e.delegateTarget).find('input[type=text]').focus();
                $('body').one('click', function (e) {
                    $('.toolbar__links .search').removeClass('show');
                });
            }
        }
    });

    $('.toolbar__links .search').on('click', 'input', function (e) {
        e.stopPropagation();
    });
    
    var current_breakpoint = getCurrentBreakpoint();

    $(window).on('load', function() {
        updateUI();
        $('svg').wrap('<div aria-hidden="true"></div>');
    });

    $(window).on('resize', function() {

        var _cb = getCurrentBreakpoint();
        
        if (current_breakpoint !== _cb) {
            current_breakpoint = _cb;
            updateUI();
        }
        
    });

    function updateUI() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            isMediumView = false;
            isLargeView = false;
            equalizeHeights('.sidebar-modules .module');
            equalizeHeights('.modules-top .module');
            equalizeHeights('.modules-top2 .module');
            equalizeHeights('.modules-bottom .module');
            $('.header-address', '#banner').prependTo('.header-name', '#banner');
            $('.header-image-1').prependTo('.header-address', '#banner');
            $('.header-image-2').prependTo('.header-welcome .message');
            $('#searchbar').insertAfter('#fonts');
            $('#content').prependTo('#content-mobile');
            $('.header__title, .header__address').prependTo('.header__holder--mobile');
            $('#sidebar-menu').prependTo('#sidebar-menu-mobile');
            $('.modules-top').prependTo('#modules-top-mobile');
            $('#modules-top2').prependTo('#modules-top2-mobile');
            $('#modules-bottom').prependTo('#modules-bottom-mobile');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('.header-address', '#banner').insertAfter('.header-name', '#banner');
            $('.header-image-1').insertAfter('.header-address', '#banner');
            $('.header-image-2').insertAfter('.header-address', '#banner');
            $('#searchbar').prependTo('#searchbar-desktop');
            $('#content').prependTo('#content-desktop');
            $('.header__title, .header__address').prependTo('.header__holder--desktop');
            $('#sidebar-menu').prependTo('#sidebar-menu-desktop');
            $('.modules-top').prependTo('#modules-top-desktop');
            $('#modules-top2').prependTo('#modules-top2-desktop');
            $('#modules-bottom').prependTo('#modules-bottom-desktop');
        }
        if (Modernizr.mq('(min-width: 1200px)') && !isLargeView) {
            isLargeView = true;
            isMediumView = false;
            isMobileView = false;
            equalizeHeights('.modules-top .module');
            equalizeHeights('.modules-top2 .module');
            equalizeHeights('.modules-bottom .module');
        }
        else if (Modernizr.mq('(min-width: 992px)') && Modernizr.mq('(max-width: 1199px)') && !isMediumView) {
            isMediumView = true;
            isLargeView = false;
            isMobileView = false;
            equalizeHeights('.modules-top .module');
            equalizeHeights('.modules-top2 .module');
            equalizeHeights('.modules-bottom .module');
        }
    }

    $('#navbar-top').on('click', '.dropdown-submenu > a', function (e) {

        var $this = $(this);
        var $submenu = $this.next('.dropdown-menu');

        if ($submenu.length > 0) {
            if (isMobile.any) {

                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }

                $this.parent().addClass('open');

            } else {
                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }
            }
        }

    });

    $('#sidebar-menu').on('click', 'a', function (e) {
        $(window).trigger('resize');

        var $this = $(this);
        var $sublist = $this.next('.dropdown-menu');

        if ($sublist.is(':hidden')) {
            e.preventDefault();
            $sublist.show();
        }
        else if (!$sublist.is(':hidden')) {
            window.location.href = $this.attr('href');
        }

    });

    $('.menus a.selected', '#sidebar-menu').parents('.dropdown-menu').show();
    
});

function getCurrentBreakpoint() {
    
    var breakpoints = [0, 768, 992, 1200].reverse();
    
    for (var i=0; i < breakpoints.length; i++) {
        if (Modernizr.mq('(min-width: ' + breakpoints[i] + 'px)')) {
            return i;
        }
    }
}
