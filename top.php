<div id="popup"></div>
<div class="top">
    <header class="header">
        <div class="header__objects">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="header__objects--1" width="158px" height="98px">
                <path fill-rule="evenodd" d="M157.022,78.431 C156.981,90.113 147.916,98.235 136.546,97.101 C136.546,97.113 136.546,97.125 136.546,97.138 C98.741,92.493 60.703,90.674 22.062,96.156 C22.062,96.147 22.062,96.137 22.062,96.128 C10.331,97.643 0.832,90.226 0.885,78.430 C0.919,69.787 5.846,61.523 12.758,57.121 C12.722,56.716 12.678,56.314 12.667,55.898 C12.452,45.507 19.923,35.716 29.654,34.682 C31.709,34.467 33.702,34.679 35.591,35.220 C38.473,23.529 48.099,14.586 60.169,14.301 C65.765,14.157 71.079,15.874 75.623,18.925 C81.427,6.896 94.368,-0.403 109.378,0.697 C130.918,2.125 149.434,21.493 149.617,42.236 C149.685,48.258 148.252,53.766 145.756,58.545 C152.559,63.005 157.041,70.459 157.022,78.431 ZM137.640,42.009 C137.419,24.743 121.904,9.317 103.492,8.443 C90.703,7.789 79.712,14.206 74.550,24.293 C70.703,21.831 66.161,20.478 61.344,20.674 C50.956,21.078 42.443,28.646 39.604,38.429 C37.981,37.993 36.257,37.830 34.465,38.024 C25.969,38.948 19.162,47.213 19.048,56.020 C19.045,56.373 19.071,56.714 19.090,57.058 C12.933,60.834 8.433,67.912 8.130,75.271 C7.707,85.332 15.586,91.726 25.845,90.508 C25.844,90.516 25.844,90.524 25.844,90.533 C59.634,85.919 93.027,86.368 126.347,88.873 C126.347,88.862 126.347,88.852 126.347,88.841 C136.358,89.322 144.366,82.237 144.300,72.360 C144.246,65.638 140.269,59.416 134.373,55.815 C136.516,51.732 137.719,47.060 137.640,42.009 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="header__objects--2" width="65px" height="64px">
                <path fill-rule="evenodd" d="M58.455,21.955 C55.126,23.538 51.281,26.078 49.516,28.934 C54.033,38.235 60.757,47.558 65.000,56.500 C55.592,52.270 46.288,47.597 36.844,45.427 C29.902,51.697 24.968,58.463 18.579,63.464 C17.949,59.064 21.148,48.699 22.877,40.734 C17.121,35.743 7.735,37.002 -0.000,35.500 C7.738,28.759 18.631,23.889 26.518,17.907 C24.611,11.597 29.001,4.008 29.000,0.500 C34.552,2.550 37.555,6.742 41.396,13.741 C49.331,13.419 55.948,13.270 63.949,16.861 C64.543,19.205 61.779,20.325 58.455,21.955 ZM54.032,17.513 C48.564,16.355 44.018,17.229 38.516,17.797 C35.816,12.762 33.608,8.537 29.677,6.424 C29.823,10.605 26.812,16.312 28.166,20.923 C22.673,25.052 15.045,28.185 9.729,33.000 C15.087,33.911 21.633,33.501 25.640,37.081 C24.425,42.718 22.208,50.609 22.649,54.507 C27.049,50.361 30.519,44.710 35.371,40.189 C41.956,41.399 48.445,43.860 54.966,47.038 C51.953,39.990 47.311,34.334 44.178,28.187 C46.577,23.599 54.808,20.139 54.032,17.513 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="header__objects--3" width="139px" height="104px">
                <path fill-rule="evenodd" d="M122.394,66.674 C122.445,67.218 122.490,67.766 122.497,68.312 C122.659,79.151 113.835,86.243 102.519,85.281 C97.012,94.813 86.641,103.141 75.214,103.624 C65.753,104.040 57.532,99.162 51.877,92.498 C46.721,96.524 40.789,99.359 34.497,100.352 C15.597,102.999 0.139,90.943 0.122,72.739 C0.140,58.835 8.893,45.815 21.128,40.055 C19.585,37.918 18.670,35.296 18.629,32.402 C18.519,23.744 25.916,16.476 35.114,16.327 C38.032,16.260 40.784,16.886 43.203,17.989 C49.506,7.899 61.625,0.508 75.544,0.483 C94.526,0.649 110.392,14.067 113.659,29.391 C115.207,29.203 116.820,29.155 118.493,29.319 C129.993,30.077 139.081,41.188 138.985,53.269 C138.927,64.202 131.953,68.623 122.394,66.674 ZM117.498,31.530 C115.911,31.400 114.378,31.455 112.906,31.632 C110.420,18.243 95.875,7.610 78.589,8.489 C66.290,9.140 55.821,15.463 50.414,23.832 C48.381,23.058 46.071,22.686 43.633,22.872 C36.022,23.403 30.003,29.404 30.124,36.083 C30.168,38.309 30.915,40.297 32.166,41.904 C22.398,46.642 15.619,56.601 15.682,67.103 C15.779,80.795 27.792,90.199 42.927,88.081 C48.051,87.337 52.911,85.261 57.160,82.343 C61.887,87.360 68.903,90.810 76.985,90.699 C86.917,90.572 95.973,85.035 101.005,77.865 C111.338,78.989 119.777,73.626 119.953,64.638 C119.962,64.186 119.935,63.732 119.903,63.280 C129.133,64.963 136.260,61.381 136.641,52.140 C137.087,41.854 128.529,32.122 117.498,31.530 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="header__objects--4" width="32px" height="39px">
                <path fill-rule="evenodd" d="M30.244,34.420 C25.568,33.314 20.785,28.661 17.478,26.703 C17.553,29.398 18.537,35.144 17.063,38.778 C13.343,36.850 9.860,34.726 6.859,32.203 C7.869,28.967 10.474,23.938 11.170,20.824 C8.400,18.492 3.668,18.109 0.554,15.917 C1.353,12.752 2.636,9.189 4.153,5.431 C7.745,7.215 10.083,11.763 12.224,14.427 C13.653,10.820 15.691,4.825 18.834,0.155 C22.024,2.359 25.109,4.628 28.643,6.622 C23.346,12.611 18.763,15.939 17.523,19.443 C20.414,21.643 26.217,24.283 31.991,24.718 C30.616,28.437 30.479,31.399 30.244,34.420 L30.244,34.420 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="header__objects--5" width="79px" height="73px">
                <path fill-rule="evenodd" d="M42.778,72.623 C37.169,58.816 32.569,49.735 22.554,42.607 C13.691,35.076 -0.586,29.499 1.047,13.392 C1.832,7.476 4.734,3.689 8.658,1.714 C10.620,0.726 12.831,0.191 15.136,0.081 C15.713,0.054 16.296,0.053 16.882,0.077 C17.469,0.102 18.060,0.152 18.653,0.227 C19.839,0.377 21.033,0.626 22.222,0.963 C31.791,3.904 40.923,11.359 41.131,19.032 C41.735,15.226 49.261,10.143 58.019,7.180 C66.658,4.305 75.443,4.427 78.015,14.519 C80.769,29.107 69.862,34.598 62.274,42.894 C53.812,50.619 48.670,61.149 42.778,72.623 ZM67.921,9.187 C54.350,7.459 45.464,16.076 41.316,21.684 C39.706,15.776 29.872,8.975 20.755,5.967 C11.931,3.049 3.534,6.795 6.921,22.774 C9.821,30.064 19.117,34.003 25.712,39.689 C32.882,45.243 37.350,52.545 43.889,63.519 C49.162,53.228 56.057,42.453 65.406,36.314 C69.834,32.786 73.716,28.713 74.455,23.728 C75.235,18.795 73.281,13.442 67.921,9.187 L67.921,9.187 Z"/>
            </svg>
        </div>
        <div id="banner" class="banner">
            <?php if ($outBannerTopRows == 0): ?>
                <?php
                    $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                ?>
                <div class="carousel-content">
                    <div class="banner-photo">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1202 555">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="banner-filter" x="0px" y="0px" width="1202px" height="555px">
                                    <feOffset in="SourceAlpha" dx="0.209" dy="1.989" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.16"/></feComponentTransfer>
                                    <feMerge>
                                        <feMergeNode/>
                                        <feMergeNode in="SourceGraphic"/>
                                    </feMerge>
                                </filter>
                                <pattern id="banner-image" patternUnits="objectBoundingBox" width="100%" height="100%">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                </pattern>
                            </defs>
                            <g filter="url(#banner-filter)">
                                <path fill-rule="evenodd" fill="url(#banner-image)" d="M70.049,51.000 L1191.950,51.000 C1197.473,51.000 1201.950,55.477 1201.950,61.000 L1201.950,552.000 C1201.950,557.523 1202.473,502.000 1131.950,502.000 L10.050,502.000 C7.193,502.000 0.049,500.117 0.049,492.000 L0.049,1.000 C0.049,-4.523 1.527,51.000 70.049,51.000 Z"/>
                            </g>
                        </svg>
                    </div>
                </div>
            <?php elseif ($outBannerTopRows == 1): ?>
                <?php
                    $value = $outBannerTop[0];
                    $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . urldecode($value['photo']);
                ?>
                <div class="carousel-content">
                    <div class="banner-photo">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1202 555">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="banner-filter" x="0px" y="0px" width="1202px" height="555px">
                                    <feOffset in="SourceAlpha" dx="0.209" dy="1.989" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.16"/></feComponentTransfer>
                                    <feMerge>
                                        <feMergeNode/>
                                        <feMergeNode in="SourceGraphic"/>
                                    </feMerge>
                                </filter>
                                <pattern id="banner-image" patternUnits="objectBoundingBox" width="100%" height="100%">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                </pattern>
                            </defs>
                            <g filter="url(#banner-filter)">
                                <path fill-rule="evenodd" fill="url(#banner-image)" d="M70.049,51.000 L1191.950,51.000 C1197.473,51.000 1201.950,55.477 1201.950,61.000 L1201.950,552.000 C1201.950,557.523 1202.473,502.000 1131.950,502.000 L10.050,502.000 C7.193,502.000 0.049,500.117 0.049,492.000 L0.049,1.000 C0.049,-4.523 1.527,51.000 70.049,51.000 Z"/>
                            </g>
                        </svg>
                    </div>
                </div>
            <?php else: $i; ?>
                <div class="carousel-content owl-carousel">
                    <?php foreach ($outBannerTop as $value): ?>
                        <?php
                            $i++;
                            $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . urldecode($value['photo']);
                        ?>
                        <div class="banner-photo">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1202 555">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="<?php echo 'banner-filter-' . $i; ?>" x="0px" y="0px" width="1202px" height="555px">
                                        <feOffset in="SourceAlpha" dx="0.209" dy="1.989" />
                                        <feGaussianBlur result="blurOut" stdDeviation="0" />
                                        <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.16"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                    <pattern id="<?php echo 'banner-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                    </pattern>
                                </defs>
                                <g filter="<?php echo 'url(#banner-filter-' . $i . ')'; ?>">
                                    <path fill-rule="evenodd" fill="<?php echo 'url(#banner-image-' . $i . ')'; ?>" d="M70.049,51.000 L1191.950,51.000 C1197.473,51.000 1201.950,55.477 1201.950,61.000 L1201.950,552.000 C1201.950,557.523 1202.473,502.000 1131.950,502.000 L10.050,502.000 C7.193,502.000 0.049,500.117 0.049,492.000 L0.049,1.000 C0.049,-4.523 1.527,51.000 70.049,51.000 Z"/>
                                </g>
                            </svg>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
        </div>
        <div class="header__holder--mobile"></div>
    </header>
    <div class="page-content page-content-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="content-mobile"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="sidebar-menu-mobile" class="sidebar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
