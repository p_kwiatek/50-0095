<?php if ($numRowBottomModules > 0): ?>
<div id="modules-bottom" class="modules-bottom">
    <svg class="modules-bottom__object" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="88px" height="91px">
        <path fill-rule="evenodd" d="M16.649,90.663 C15.627,70.640 7.839,63.026 3.401,51.016 C-1.092,39.874 -2.234,24.337 8.752,8.527 C13.669,2.809 19.218,0.587 24.546,0.878 C27.210,1.021 29.820,1.787 32.267,3.047 C32.879,3.362 33.481,3.708 34.071,4.083 C34.661,4.458 35.239,4.861 35.803,5.291 C36.931,6.150 38.005,7.115 39.011,8.163 C47.106,17.114 50.495,28.984 43.511,34.616 C48.100,31.668 60.570,31.363 71.345,33.953 C82.286,36.615 90.610,41.410 85.719,48.847 C78.606,61.308 68.357,58.381 54.850,63.729 C41.793,67.690 25.479,79.926 16.649,90.663 ZM79.826,40.388 C67.766,32.272 52.189,33.463 41.263,36.819 C44.519,31.958 40.948,21.044 33.878,12.380 C26.814,4.275 16.331,3.014 6.342,22.000 C3.247,31.465 4.594,41.348 7.983,49.406 C11.340,57.997 16.738,64.763 21.008,80.331 C30.824,70.357 49.421,58.749 61.878,58.679 C68.230,57.842 73.440,56.705 76.897,53.339 C80.298,50.005 82.073,45.669 79.826,40.388 Z"/>
    </svg>
    <div class="modules-list">
    <?php
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
        
        $links = array();
        
        for ($i = 0; $i < $numRowBottomModules; $i++) {
            $tmp = get_module($outRowBottomModules[$i]['mod_name']);
            
            preg_match($href, $tmp, $m);

            if ($m[2] != '') {
                $links[] = $m[2];
            } else {
                $links[] = trans_url_name($outRowBottomModules[$i]['name']);
            }
        }
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $module_grid_classes = array(1 => "col-sm-12 col-md-12", 2 => "col-sm-6 col-md-6", 3 => "col-sm-4 col-md-4");
        $module_grid_class = array_key_exists($numRowBottomModules, $module_grid_classes) ? $module_grid_classes[$numRowBottomModules] : $module_grid_classes[3];
    ?>

    <?php for ($i = 0; $i < $numRowBottomModules; $i++): ?>
        <div class="module <?php echo $module_grid_class; ?> module-common <?php echo in_array($outRowBottomModules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowBottomModules[$i]['mod_name']; ?>">
            <svg class="vector-decoration" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
            </svg>
            <div class="modules-bottom__holder">
                <div class="modules-bottom__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="79px" height="79px" class="modules-bottom__icon--background">
                        <defs>
                            <filter id="<?php echo 'module-bottom-icon-' . $i; ?>">
                                <feOffset in="SourceAlpha" dx="0" dy="0" />
                                <feGaussianBlur result="blurOut" stdDeviation="3.162" />
                                <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.15"/></feComponentTransfer>
                                <feBlend mode="normal" in2="SourceGraphic" />
                            </filter>
                        </defs>
                        <g filter="<?php echo 'url(#module-bottom-icon-' . $i . ')'; ?>">
                            <path fill-rule="evenodd" d="M39.516,73.074 C33.816,73.074 28.451,71.648 23.749,69.141 L6.000,73.000 L9.863,55.229 C7.373,50.541 5.957,45.194 5.957,39.516 C5.957,20.982 20.982,5.957 39.516,5.957 C58.050,5.957 73.074,20.982 73.074,39.516 C73.074,58.049 58.050,73.074 39.516,73.074 Z"/>
                        </g>
                    </svg>
                    <?php if(array_key_exists($outRowBottomModules[$i]['mod_name'], $icons)): echo $icons[$outRowBottomModules[$i]['mod_name']]; endif; ?>
                </div>
                <h2 class="modules-bottom__name name-<?php echo ($i + 1); ?>"><?php echo $outRowBottomModules[$i]['name'] ?></h2>
                <div class="modules-bottom__content"><?php echo get_module($outRowBottomModules[$i]['mod_name']); ?></div>
            </div>
        </div>
    <?php endfor; ?>
    </div>
</div>
<?php endif; ?>
