<div class="sidebar">
    <div id="modules-top2-mobile"></div>
    <div id="sidebar-menu-desktop">
        <div id="sidebar-menu" class="sidebar-menu clearfix">
            <h2><?php echo __('Menu') ?></h2>
            <a id="add-menu" class="anchor" tabindex="-1"></a>
            <?php
                $caretLeft = '';
            ?>
            
            <?php get_menu_tree('mg', 0, 0, '', false, '', false, true, true, false, true, true, $caretLeft, true); ?>
            <?php
            /*
             * Dynamic menus
             */
            foreach ($menuType as $dynMenu) {
                if ($dynMenu['menutype'] != 'mg' && $dynMenu['menutype'] != 'tm') {
                    if ($dynMenu['active'] == 1) {

                        $menuClass = '';
                        if (strlen($dynMenu['name']) > 30) {
                            $menuClass = ' module-name-3';
                        } else if (strlen($dynMenu['name']) > 17 && strlen($dynMenu['name']) <= 30) {
                            $menuClass = ' module-name-2';
                        } else {
                            $menuClass = ' module-name-1';
                        }
                        
                        $caretLeft = '';
                        
                        ?>
                        <h2 class="module-name <?php echo $menuClass ?>"><?php echo $dynMenu['name'] ?></h2>
                        <?php get_menu_tree($dynMenu['menutype'], 0, 0, '', false, '', false, true, true, false, true, true, $caretLeft, true); ?>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="sidebar-modules">
    <?php
        foreach ($outRowLeftModules as $module) {
        
        $modules_with_icons = array(
            'mod_calendar',
            'mod_stats',
            'mod_kzk',
            'mod_location',
            'mod_timetable',
            'mod_menu',
            'mod_newsletter',
            'mod_gallery',
            'mod_video',
            'mod_jokes',
            'mod_programs',
            'mod_forum',
            'mod_contact',
            'mod_login',
        );
        
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';

        $link = '';

        $tmp = get_module($module['mod_name']);

        preg_match($href, $tmp, $m);

        if ($m[2] != '') {
            $link = $m[2];
        } else {
            $link = trans_url_name($module['name']);
        }
        
	?>
        <div class="<?php echo in_array($module['mod_name'], $modules_with_icons) ? 'module module--icon' : 'module'; ?>" id="<?php echo $module['mod_name']; ?>">
            <div class="module-body clearfix">
                <?php if (in_array($module['mod_name'], $modules_with_icons)): ?>
                    <svg class="vector-decoration top" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                        <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    </svg>
                    <svg class="vector-decoration bottom" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                        <path fill-rule="evenodd" fill="#000" opacity="0.1" transform="translate(0,3)" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                        <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    </svg>
                    <h2 class="module-name"><?php echo $module['name'] ?></h2>
                    <div class="module-content">
                        <div class="content"><?php echo get_module($module['mod_name']) ?></div>
                        <div class="icon">
                            <svg class="icon-background" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 79 79">
                                <defs>
                                    <filter id="<?php echo 'sidebar-module-filter-' . $module['mod_name']; ?>">
                                        <feOffset in="SourceAlpha" dx="0" dy="0" />
                                        <feGaussianBlur result="blurOut" stdDeviation="3.162" />
                                        <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                        <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                        <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.15"/></feComponentTransfer>
                                        <feBlend mode="normal" in2="SourceGraphic" />
                                    </filter>
                                </defs>
                                <g filter="<?php echo 'url(#sidebar-module-filter-' . $module['mod_name'] . ')'; ?>">
                                    <path fill-rule="evenodd" d="M39.516,73.074 C33.816,73.074 28.451,71.648 23.749,69.141 L6.000,73.000 L9.863,55.229 C7.373,50.541 5.957,45.194 5.957,39.516 C5.957,20.982 20.982,5.957 39.516,5.957 C58.049,5.957 73.074,20.982 73.074,39.516 C73.074,58.049 58.049,73.074 39.516,73.074 Z"/>
                                </g>
                            </svg>
                            <?php if(array_key_exists($module['mod_name'], $icons)): echo $icons[$module['mod_name']]; endif; ?>
                        </div>
                    </div>
                <?php else: ?>
                    <svg class="vector-decoration top" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                        <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    </svg>
                    <svg class="vector-decoration bottom" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                        <path fill-rule="evenodd" fill="#000" opacity="0.1" transform="translate(0,3)" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                        <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    </svg>
                    <h2 class="module-name"><?php echo $module['name'] ?></h2>
                    <div class="module-content"><?php echo get_module($module['mod_name']) ?></div>
                <?php endif; ?>
            </div>

        </div>
    <?php
        }
    ?>
    </div>
    
    <div id="modules-bottom-mobile"></div>
    
    <?php
    if (count($leftAdv) > 0)
    {
	?>
	<div id="advertsLeftWrapper">
	    <div id="advertsLeftContent">
	<?php
	$n = 0;
	foreach ($leftAdv as $adv)
	{
	    ?>
	    <div class="advertLeft">
	    <?php
	    $extUrl = '';
	    if ($adv['ext_url'] != '')
	    {
		$newWindow = '';
		if ($adv['new_window'] == 1)
		{
		    $newWindow = ' target="_blank" ';
		}
	    			
		$swfLink = '';
		$swfSize = '';
		if ($adv['attrib'] != '' && $adv['type'] == 'flash')
		{
		    //$swfLink = ' style="width:'.$swfSize[0].'px; height:'.$swfSize[1].'px; z-index:1; display:block; position:absolute; left:0; top:0" ';
		}
					
		$extUrl = '<a href="'.$adv['ext_url'].'"'.$newWindow.$swfLink.'>';
		$extUrlEnd = '</a>';
	    }
				
	    switch ($adv['type'])
	    {
		case 'flash':
		    $swfSize = explode(',', $adv['attrib']);
		    //echo $extUrl;
		    ?>
		    <div id="advLeft_<?php echo $n?>"><?php echo $adv['attrib']?></div>
		    <script type="text/javascript">
                        swfobject.embedSWF("<?php echo $adv['content'] . '?noc=' . time()?>", "advLeft_<?php echo $n?>", "<?php echo $swfSize[0]?>", "<?php echo $swfSize[1]?>", "9.0.0", "expressInstall.swf", {}, {wmode:"transparent"}, {});
		    </script>
		    <?php			
		    //echo $extUrlEnd;
		    break;
					
		case 'html':
		    echo $adv['content'];
		    break;
					
		case 'image':
		default:
		    $width = '';
		    if ($adv['content'] != '')
		    {                    
                        $size = getimagesize($adv['content']);
                        if ($size[0] > $templateConfig['maxWidthLeftAdv'])
                        {
                            $width = ' width="'.$templateConfig['maxWidthLeftAdv'].'" ';
                        } 
                        echo $extUrl;
                        ?>
                        <img src="<?php echo $adv['content']?>" alt="<?php echo $adv['name']?>" <?php echo $width?>/>
                        <?php
                        echo $extUrlEnd;
		    }
		    break;
	    }
	    ?>
	    </div>
	    <?php
	    $n++;
	}
	?>
	    </div>
	</div>
        
	<?php
    }
    ?>

</div>
