<?php
include_once ( CMS_TEMPL . DS . 'i18n' . DS . $lang . '.php');
include_once ( CMS_TEMPL . DS . 'header.php');
include_once ( CMS_TEMPL . DS . 'toolbar.php');
include_once ( CMS_TEMPL . DS . 'top.php');
?>
<div id="modules-top-desktop">
    <?php include_once('modules_top.php'); ?>
</div>
<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3 aside">
                <?php include_once ( CMS_TEMPL . DS . 'left.php'); ?>
            </div>
            <div class="col-xs-12 col-md-9 main">
                <svg class="content-vector-decoration" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 53.39">
                    <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z" class="content-vector-decoration__shadow" transform="translate(0,3)" opacity="0.1"></path>
                    <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"></path>
                 </svg>
                <div class="row">
                    <div class="col-md-12">
                        <div id="modules-top2-desktop">
                            <?php include_once ( CMS_TEMPL . DS . 'modules_top2.php'); ?>
                        </div>

                        <div id="content-desktop">
                            <div id="content">
                                <?php
                                if ($_GET['c'] == '') {
                                    include_once( CMS_TEMPL . DS . 'topAdv.php');
                                }

                                $crumbpathSep = '<i class="icon-right-open-big icon" aria-hidden="true"></i>';
                                ?>

                                <a id="link-content" tabindex="-1"></a>

                                <div id="crumbpath"><span class="here"><?php echo __('you are here'); ?>:</span><ol class="list-unstyled list-inline"><?php echo show_crumbpath($crumbpath, $crumbpathSep); ?></ol></div>

                                <div id="content_txt">
                                    <?php include_once ( $TEMPL_PATH ); ?>
                                </div>

                            </div>
                        </div>

                        <div id="modules-bottom-desktop">
                            <?php include_once('modules_bottom.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once ( CMS_TEMPL . DS . 'footer.php'); ?>