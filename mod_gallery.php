<?php
if ($showAll)
{
    ?>
<div class="gallery-wrapper">
    <h2 class="main-header"><span><?php echo $pageName?></span></h2>
    <ul class="gallery">
    <?php
    if (count($albums) > 0)
    {
        $n = 0;
        foreach ($albums as $value)
        {
            $n++;
            ?>
            <li>
                <a href="<?php echo $value['link']?>" class="photo">
                    <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 370 229">
                        <defs>
                            <pattern id="<?php echo 'article-image-' . $n; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']; ?>"></image>
                            </pattern>
                        </defs>
                        <path fill-rule="evenodd" stroke-width="2px" fill="<?php echo 'url(#article-image-' . $n . ')'; ?>" d="M10.000,5.000 L360.000,5.000 C362.761,5.000 365.000,7.238 365.000,10.000 L365.000,219.000 C365.000,221.761 362.761,224.000 360.000,224.000 L10.000,224.000 C7.239,224.000 5.000,221.761 5.000,219.000 L5.000,10.000 C5.000,7.238 7.239,5.000 10.000,5.000 Z"/>
                    </svg>
                    <p class="photo-name"><?php echo $value['name']?></p> 
                </a>
            </li>
            <?php
        }	
    } else
    {
        ?>
        <p><?php echo __('no photo album added')?></p>
        <?php
    }
    ?>
</div>
    <?php
}
if ($showOne)
{
    ?>
<div class="gallery-wrapper">
    <h2 class="main-header"><span><?php echo $pageName?></span></h2>
        <?php 
        echo $message;
        ?>
	<?php 
	if ($showGallery)
	{
	    if (count($outRows) > 0)
	    {
                ?>
                <ul class="gallery">
                <?php
		$n = 0;
		foreach ($outRows as $value)
		{
		    $n++;
		    ?>
		    <li>
    			<a href="files/<?php echo $lang?>/<?php echo $value['file']?>" title="<?php echo __('enlarge image') . ': ' . $value['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                    <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 370 229">
                        <defs>
                            <pattern id="<?php echo 'article-image-' . $n; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']; ?>"></image>
                            </pattern>
                        </defs>
                        <path fill-rule="evenodd" stroke-width="2px" fill="<?php echo 'url(#article-image-' . $n . ')'; ?>" d="M10.000,5.000 L360.000,5.000 C362.761,5.000 365.000,7.238 365.000,10.000 L365.000,219.000 C365.000,221.761 362.761,224.000 360.000,224.000 L10.000,224.000 C7.239,224.000 5.000,221.761 5.000,219.000 L5.000,10.000 C5.000,7.238 7.239,5.000 10.000,5.000 Z"/>
                    </svg> 
        			<?php
        			if (! check_html_text($value['name'], '') )
        			{
        			    ?>
        			    <p class="photo-name" aria-hidden="true"><?php echo $value['name']?></p>
        			    <?php
        			}
        		    ?>
                </a>
		    </li>
                <?php
		}
                ?>
                </ul>
                <?php
	    }
        }
    ?>
</div>
<?php
if ($showLoginForm)
{
    ?>
    <div class="main-text">
    <?php
    include( CMS_TEMPL . DS . 'form_login.php');
    ?>
    </div>
    <?php
}
?>
    
<?php
}
?>
