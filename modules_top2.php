<div id="modules-top2" class="modules-top2">
    <?php if ($numRowTop2Modules > 0): ?>
        <?php
            $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
            
            $links = array();
            
            for ($i = 0; $i < $numRowTop2Modules; $i++) {
                $tmp = get_module($outRowTop2Modules[$i]['mod_name']);
                
                preg_match($href, $tmp, $m);

                if ($m[2] != '') {
                    $links[] = $m[2];
                } else {
                    $links[] = trans_url_name($outRowTop2Modules[$i]['name']);
                }
            }
            
            $modules_color2 = array(
                'mod_forum',
            );
            
            $module_grid_classes = array(1 => "module-1", 2 => "module-2", 3 => "module-3");
            $module_grid_class = array_key_exists($numRowTop2Modules, $module_grid_classes) ? $module_grid_classes[$numRowTop2Modules] : $module_grid_classes[3];
        ?>
        
        <?php for ($i = 0; $i < $numRowTop2Modules; $i++): ?>
            <div class="module <?php echo $module_grid_class; ?> module-common <?php echo in_array($outRowTop2Modules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowTop2Modules[$i]['mod_name']; ?>">
                <svg class="vector-decoration" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                    <path fill-rule="evenodd" fill="#000" opacity="0.1" transform="translate(0,3)" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                </svg>
                <div class="modules-top2__holder">
                    <div class="modules-top2__icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="79px" height="79px" class="modules-top2__icon--background">
                            <defs>
                                <filter id="<?php echo 'module-top2-icon-' . $i; ?>">
                                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                                    <feGaussianBlur result="blurOut" stdDeviation="3.162" />
                                    <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                    <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                    <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.15"/></feComponentTransfer>
                                    <feBlend mode="normal" in2="SourceGraphic" />
                                </filter>
                            </defs>
                            <g filter="<?php echo 'url(#module-top2-icon-' . $i . ')'; ?>">
                                <path fill-rule="evenodd" d="M39.516,73.074 C33.816,73.074 28.451,71.648 23.749,69.141 L6.000,73.000 L9.863,55.229 C7.373,50.541 5.957,45.194 5.957,39.516 C5.957,20.982 20.982,5.957 39.516,5.957 C58.050,5.957 73.074,20.982 73.074,39.516 C73.074,58.049 58.050,73.074 39.516,73.074 Z"/>
                            </g>
                        </svg>
                        <?php if(array_key_exists($outRowTop2Modules[$i]['mod_name'], $icons)): echo $icons[$outRowTop2Modules[$i]['mod_name']]; endif; ?>
                    </div>
                    <h2 class="modules-top2__name name-<?php echo $i; ?>"><?php echo $outRowTop2Modules[$i]['name'] ?></h2>
                    <div class="modules-top2__content"><?php echo get_module($outRowTop2Modules[$i]['mod_name']); ?></div>
                </div>
            </div>
        <?php endfor; ?>
    <?php endif; ?>
</div>