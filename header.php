<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<!--[if lte IE 8]>
<script type="text/javascript">
    window.location = "<?php echo $pathTemplate?>/ie8.php";
</script>
<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?php echo $pageTitle; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<meta name="description" content="<?php echo $pageDescription; ?>" />
<meta name="keywords" content="<?php echo $pageKeywords; ?>" />
<meta name="author" content="<?php echo $cmsConfig['cms']; ?>" />
<meta name="revisit-after" content="3 days" />
<meta name="robots" content="all" />
<meta name="robots" content="index, follow" />
<?php

array_unshift($css, 'vendor/bootstrap/css/bootstrap.min.css');

$jquery = array_shift($js);
array_unshift($js, 'owl.carousel.min.js');
array_unshift($js, 'ismobile.js');
array_unshift($js, 'modernizr.js');
array_unshift($js, 'what-input.min.js');
array_unshift($js, 'vendor/snap/snap.svg.min.js');
array_unshift($js, 'vendor/bootstrap/js/bootstrap.min.js');
array_unshift($js, $jquery);

$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
	
foreach ($js as $k => $v) {
    if (strpos($v, 'vendor') !== FALSE) {
        $path = $pathTemplate .'/' . $v;
    } else {
        $path = $pathTemplate .'/js/' . $v;
    }
    
    echo '<script type="text/javascript" src="'. $path . '"></script>' . "\r\n";
    
}
?>

<?php
foreach ($css as $k => $v) {
    if (strpos($v, 'vendor') !== FALSE) {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/' . $v . '"/>' . "\r\n";
    } else {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/css/' . $v . '"/>' . "\r\n";
    }
    
    if ($v == 'style.css')
    {
	$popupBackground = '#fff';
        $mainColor = $templateConfig['mainColor'];
        $overlayColor = $templateConfig['overColor'];
        $highColor = $templateConfig['highColor'];
			
	// wersja zalobna
	if ($outSettings['funeral'] == 'włącz') 
	{
	    $popupBackground = $templateConfig['popupBackground-bw'];
	    $mainColor = $templateConfig['mainColor-bw'];
	    $overlayColor = $templateConfig['overColor-bw'];
	    $highColor = $templateConfig['highColor-bw'];
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/style.css"/>' . "\r\n";
	}		
    }
		
    if ($v == 'jquery.fancybox.css')
    {
        if ($outSettings['funeral'] == 'włącz') 
        {
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/jquery.fancybox.css"/>' . "\r\n";
	}
    }
    if ($v == 'addition.css')
    {
	if ($outSettings['funeral'] == 'włącz')
	{
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/addition.css"/>' . "\r\n";
	}
    }      
}

$contrast = '';
if ($_SESSION['contr'] == 1)
{
    $contrast = 'flashvars.contrast = 1;' . "\r\n";
    $popupBackground = $templateConfig['popupBackground-ct'];
    $mainColor = $templateConfig['mainColor-ct'];
    $overlayColor = $templateConfig['overColor-ct'];
    $highColor = $templateConfig['highColor-ct'];	    
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/style.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/jquery.fancybox.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/addition.css"/>' . "\r\n";
}	
	
echo '<link rel="shortcut icon" href="http://' . $pageInfo['host'] . '/' . $templateDir . '/images/favicon.ico" />' . "\r\n";

switch ($_SESSION['style']) {
    case '1' :
        ?>
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo $pathTemplate?>/css/font-1.css"/>
        <?php
        break;
    case '2' :
        ?>
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo $pathTemplate?>/css/font-2.css"/>
        <?php        
        break;
}
?>
<script type="text/javascript">
    texts = {
        image: '<?php echo __('image')?>',
        enlargeImage: '<?php echo __('enlarge image')?>',
        closeGallery: '<?php echo __('close gallery')?>',
        prevGallery: '<?php echo __('prev gallery')?>',
        nextGallery: '<?php echo __('next gallery')?>'
    };
    
    settings = {
        overlayColor: '<?php echo $overlayColor; ?>',
        transition: '<?php echo $outSettings['animType']?>',
        animationDuration: <?php echo $outSettings['transition']?>,
        duration: <?php echo $outSettings['duration']?>,
        showClock: false
    };
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-3924208-1']);
    _gaq.push(['_setDomainName', '.szkolnastrona.pl']);
    _gaq.push(['_setAllowHash', false]);
    _gaq.push(['_trackPageview']);

    (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<?php 
    $icons = array(
        'mod_calendar' => '<svg id="icon-calendar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="35px" height="38px">
            <path fill-rule="evenodd" class="icon-calendar__path--1"  d="M20.185,21.546 C19.553,21.113 19.288,20.294 19.486,19.554 C19.684,18.815 20.323,18.238 21.087,18.179 C26.056,17.705 30.740,13.369 32.101,8.452 L10.169,2.576 C8.890,7.515 10.789,13.570 14.844,16.506 C15.476,16.939 15.741,17.759 15.543,18.498 C15.345,19.237 14.706,19.814 13.942,19.873 C8.973,20.347 4.290,24.683 2.929,29.599 L24.860,35.476 C26.140,30.537 24.240,24.482 20.185,21.546 Z"/>
            <path fill-rule="evenodd" class="icon-calendar__path--2"  d="M34.204,9.148 C33.962,10.051 33.076,10.562 32.214,10.331 L9.132,4.146 C8.270,3.915 7.717,3.019 7.959,2.115 C8.191,1.253 9.087,0.701 9.990,0.943 L33.031,7.117 C33.894,7.348 34.447,8.244 34.204,9.148 ZM27.040,35.884 C26.798,36.788 25.912,37.299 25.050,37.068 L1.968,30.883 C1.106,30.652 0.553,29.756 0.795,28.852 C1.026,27.990 1.923,27.437 2.826,27.680 L25.867,33.853 C26.730,34.084 27.283,34.981 27.040,35.884 Z"/>
            <path fill-rule="evenodd" class="icon-calendar__path--3"  d="M16.101,14.773 C17.171,15.545 17.616,16.676 17.626,17.955 C17.626,17.955 17.392,18.993 17.114,20.194 C16.458,21.163 15.394,21.847 14.175,21.961 C11.282,22.242 8.330,24.224 6.511,26.907 L23.104,31.353 C22.869,28.120 21.304,24.928 18.940,23.238 C17.910,22.477 17.301,21.302 17.281,20.063 C17.578,18.955 17.872,18.021 17.872,18.021 C18.520,16.919 19.563,16.142 20.866,16.050 C23.076,15.850 25.293,14.639 27.035,12.905 L12.331,8.965 C12.972,11.338 14.286,13.495 16.101,14.773 Z"/></svg>',
        'mod_stats' => '<svg id="icon-stats" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38px" height="21px">
            <path fill-rule="evenodd" class="icon-stats__path--1" d="M22.964,18.202 L20.835,16.088 L33.308,3.522 L35.437,5.636 L22.964,18.202 Z"/>
            <path fill-rule="evenodd" class="icon-stats__path--2" d="M22.580,16.162 L20.301,18.112 L12.752,9.288 L15.031,7.338 L22.580,16.162 Z"/>
            <path fill-rule="evenodd" class="icon-stats__path--3" d="M4.240,17.072 L2.711,14.491 L14.632,7.426 L16.162,10.007 L4.240,17.072 Z"/>
            <path fill-rule="evenodd" class="icon-stats__path--4" d="M4.407,12.301 C6.274,12.802 7.382,14.721 6.882,16.588 C6.382,18.455 4.462,19.563 2.595,19.063 C0.728,18.563 -0.380,16.643 0.120,14.776 C0.621,12.909 2.540,11.801 4.407,12.301 Z"/>
            <path fill-rule="evenodd" class="icon-stats__path--5" d="M15.430,5.937 C17.297,6.438 18.405,8.357 17.905,10.224 C17.404,12.091 15.485,13.199 13.618,12.699 C11.751,12.199 10.643,10.279 11.143,8.412 C11.643,6.545 13.563,5.437 15.430,5.937 Z"/>
            <path fill-rule="evenodd" class="icon-stats__path--6" d="M22.570,14.062 C24.437,14.563 25.545,16.482 25.045,18.349 C24.545,20.216 22.626,21.324 20.758,20.824 C18.891,20.324 17.783,18.404 18.284,16.537 C18.784,14.670 20.703,13.562 22.570,14.062 Z"/>
            <path fill-rule="evenodd" class="icon-stats__path--7" d="M35.405,0.937 C37.272,1.437 38.380,3.356 37.880,5.224 C37.379,7.091 35.460,8.199 33.593,7.698 C31.726,7.198 30.618,5.279 31.118,3.412 C31.618,1.545 33.537,0.437 35.405,0.937 Z"/></svg>',
        'mod_kzk' => '<svg id="icon-kzk" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40px" height="40px">
            <path fill-rule="evenodd" class="icon-kzk__path--1" d="M12.645,1.715 L34.862,7.668 C37.529,8.383 39.112,11.124 38.397,13.792 L32.444,36.008 C31.730,38.675 28.988,40.258 26.321,39.544 L4.104,33.591 C1.437,32.876 -0.146,30.134 0.569,27.467 L6.522,5.251 C7.236,2.583 9.978,1.000 12.645,1.715 Z"/>
            <path fill-rule="evenodd" class="icon-kzk__path--2" d="M7.816,0.421 L39.691,8.962 L35.550,24.417 L3.675,15.876 L7.816,0.421 Z"/>
            <path fill-rule="evenodd" class="icon-kzk__path--3" d="M36.102,20.424 C37.436,20.781 38.228,22.152 37.870,23.485 L36.835,27.349 C36.478,28.683 35.107,29.474 33.773,29.117 C32.439,28.760 31.648,27.389 32.005,26.055 L33.041,22.192 C33.398,20.858 34.769,20.066 36.102,20.424 Z"/>
            <path fill-rule="evenodd" class="icon-kzk__path--4" d="M4.227,11.883 C5.561,12.240 6.352,13.611 5.995,14.945 L4.959,18.808 C4.602,20.142 3.231,20.933 1.897,20.576 C0.564,20.219 -0.228,18.848 0.130,17.514 L1.165,13.650 C1.522,12.317 2.893,11.525 4.227,11.883 Z"/>
            <path fill-rule="evenodd" class="icon-kzk__path--5" d="M21.536,6.168 L24.899,7.069 C25.827,7.318 26.354,8.365 26.074,9.409 C25.794,10.452 24.815,11.096 23.886,10.847 L20.524,9.946 C19.595,9.698 19.069,8.650 19.349,7.607 C19.628,6.563 20.608,5.919 21.536,6.168 Z"/>
            <path fill-rule="evenodd" class="icon-kzk__path--6" d="M19.236,27.292 L22.385,15.543 L20.559,15.054 C20.269,15.571 19.960,15.949 19.632,16.188 C19.304,16.428 18.913,16.564 18.458,16.597 C18.003,16.630 17.362,16.563 16.533,16.395 L16.101,18.008 L18.953,18.773 L16.842,26.651 L19.236,27.292 Z"/></svg>',
        'mod_location' => '<svg id="icon-location" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43px" height="33px">
            <path fill-rule="evenodd" class="icon-location__path--1" d="M19.130,21.371 C28.466,23.873 35.455,28.063 34.740,30.730 C34.025,33.398 25.878,33.532 16.542,31.030 C7.207,28.529 0.218,24.339 0.933,21.671 C1.647,19.004 9.795,18.870 19.130,21.371 Z"/>
            <path fill-rule="evenodd" class="icon-location__path--2" d="M16.378,25.810 L19.249,15.095 L22.391,3.368 L22.919,1.396 C22.976,1.184 23.705,0.840 23.987,0.916 L25.188,1.237 C25.471,1.313 25.896,1.966 25.840,2.178 L25.311,4.151 L24.871,5.793 L22.609,14.236 L22.169,15.878 L19.298,26.592 L16.378,25.810 Z"/>
            <path fill-rule="evenodd" class="icon-location__path--3" d="M42.700,14.136 L36.982,17.330 L40.360,22.871 L29.354,19.922 L29.975,17.607 L28.672,10.492 L42.700,14.136 Z"/>
            <path fill-rule="evenodd" class="icon-location__path--4" d="M36.581,12.158 L34.671,19.287 L21.173,15.671 L24.534,3.126 L38.032,6.743 L36.581,12.158 Z"/></svg>',
        'mod_menu' => '<svg id="icon-menu" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="35px" height="25px">
            <path fill-rule="evenodd" class="icon-menu__path--1" d="M30.596,24.207 L24.075,20.169 C26.699,18.888 29.650,16.510 31.576,14.825 C32.026,14.432 32.358,14.005 32.580,13.557 C33.932,15.807 34.782,18.325 35.006,20.941 C35.230,23.540 32.623,25.462 30.596,24.207 ZM1.562,11.979 C2.949,9.991 4.758,8.346 6.834,7.102 C6.858,7.471 6.935,7.858 7.072,8.259 C7.916,10.732 9.323,14.365 11.002,16.789 L3.750,17.013 C1.366,17.087 0.070,14.119 1.562,11.979 Z"/>
            <path fill-rule="evenodd" class="icon-menu__path--2" d="M32.821,15.073 C29.265,18.182 22.537,23.442 19.651,21.540 C19.279,21.409 18.966,21.187 18.715,20.918 C18.880,20.761 19.027,20.582 19.144,20.374 L26.628,7.097 C26.903,6.503 26.959,5.872 26.843,5.288 C28.737,5.516 31.513,7.255 33.276,9.496 C34.760,11.381 34.645,13.478 32.821,15.073 ZM14.968,19.255 C14.966,19.510 15.005,19.758 15.080,19.991 C14.750,20.078 14.398,20.103 14.040,20.036 C10.590,20.241 7.392,12.321 5.867,7.851 C5.085,5.558 6.035,3.683 8.262,2.793 C10.867,1.751 14.080,1.618 15.853,2.328 C15.454,2.777 15.184,3.357 15.125,4.015 L14.968,19.255 Z"/>
            <path fill-rule="evenodd" class="icon-menu__path--3" d="M16.322,22.576 L16.322,22.576 C15.233,22.284 14.483,21.274 14.495,20.115 L14.669,3.184 C14.847,1.205 16.725,-0.153 18.590,0.346 L25.471,2.190 C27.337,2.690 28.283,4.805 27.449,6.608 L19.135,21.359 C18.565,22.368 17.411,22.868 16.322,22.576 Z"/></svg>',
        'mod_newsletter' => '<svg id="icon-newsletter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="29px" height="34px">
            <path fill-rule="evenodd" class="icon-newsletter__path--1" d="M28.472,13.232 L23.334,4.609 L7.944,0.486 L0.698,27.528 L23.037,33.514 L28.472,13.232 Z"/>
            <path fill-rule="evenodd" class="icon-newsletter__path--2" d="M23.099,15.411 C23.026,15.686 22.744,15.848 22.470,15.775 L10.059,12.449 C9.785,12.376 9.622,12.094 9.696,11.820 C9.769,11.545 10.051,11.383 10.325,11.456 L22.736,14.782 C23.010,14.855 23.173,15.137 23.099,15.411 ZM16.063,9.898 L11.099,8.568 C10.825,8.495 10.662,8.213 10.736,7.939 C10.809,7.664 11.091,7.502 11.365,7.575 L16.329,8.906 C16.604,8.979 16.766,9.261 16.693,9.535 C16.619,9.810 16.338,9.972 16.063,9.898 ZM22.067,19.262 C21.994,19.536 21.712,19.699 21.438,19.626 L9.027,16.300 C8.753,16.227 8.591,15.945 8.664,15.671 C8.738,15.396 9.019,15.234 9.294,15.307 L21.704,18.632 C21.978,18.706 22.141,18.987 22.067,19.262 ZM21.036,23.112 C20.962,23.387 20.681,23.550 20.406,23.476 L7.996,20.151 C7.722,20.077 7.559,19.796 7.632,19.521 C7.706,19.247 7.988,19.084 8.262,19.158 L20.672,22.483 C20.947,22.557 21.109,22.838 21.036,23.112 ZM19.996,26.994 C19.922,27.268 19.641,27.431 19.366,27.357 L6.956,24.032 C6.682,23.958 6.519,23.676 6.592,23.402 C6.666,23.127 6.948,22.965 7.222,23.039 L19.632,26.364 C19.907,26.438 20.069,26.719 19.996,26.994 Z"/>
            <path fill-rule="evenodd" class="icon-newsletter__path--3" d="M23.463,4.612 L21.633,11.441 L28.462,13.271 L23.463,4.612 Z"/></svg>',
        'mod_programs' => '<svg id="icon-programs" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="29px" height="34px">
            <path fill-rule="evenodd" class="icon-programs__path--1" d="M28.472,13.232 L23.334,4.609 L7.944,0.486 L0.698,27.528 L23.037,33.514 L28.472,13.232 Z"/>
            <path fill-rule="evenodd" class="icon-programs__path--2" d="M23.099,15.411 C23.026,15.686 22.744,15.848 22.470,15.775 L10.059,12.449 C9.785,12.376 9.622,12.094 9.696,11.820 C9.769,11.545 10.051,11.383 10.325,11.456 L22.736,14.782 C23.010,14.855 23.173,15.137 23.099,15.411 ZM16.063,9.898 L11.099,8.568 C10.825,8.495 10.662,8.213 10.736,7.939 C10.809,7.664 11.091,7.502 11.365,7.575 L16.329,8.906 C16.604,8.979 16.766,9.261 16.693,9.535 C16.619,9.810 16.338,9.972 16.063,9.898 ZM22.067,19.262 C21.994,19.536 21.712,19.699 21.438,19.626 L9.027,16.300 C8.753,16.227 8.591,15.945 8.664,15.671 C8.738,15.396 9.019,15.234 9.294,15.307 L21.704,18.632 C21.978,18.706 22.141,18.987 22.067,19.262 ZM21.036,23.112 C20.962,23.387 20.681,23.550 20.406,23.476 L7.996,20.151 C7.722,20.077 7.559,19.796 7.632,19.521 C7.706,19.247 7.988,19.084 8.262,19.158 L20.672,22.483 C20.947,22.557 21.109,22.838 21.036,23.112 ZM19.996,26.994 C19.922,27.268 19.641,27.431 19.366,27.357 L6.956,24.032 C6.682,23.958 6.519,23.676 6.592,23.402 C6.666,23.127 6.948,22.965 7.222,23.039 L19.632,26.364 C19.907,26.438 20.069,26.719 19.996,26.994 Z"/>
            <path fill-rule="evenodd" class="icon-programs__path--3" d="M23.463,4.612 L21.633,11.441 L28.462,13.271 L23.463,4.612 Z"/></svg>',    
        'mod_gallery' => '<svg id="icon-gallery" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="37px" height="38px">
            <path fill-rule="evenodd" class="icon-gallery__path--1" d="M8.351,0.763 L36.408,8.281 L28.665,37.177 L0.608,29.659 L8.351,0.763 Z"/>
            <path fill-rule="evenodd" class="icon-gallery__path--2" d="M10.522,4.412 L32.638,10.338 L27.463,29.650 L5.347,23.724 L10.522,4.412 Z"/>
            <path fill-rule="evenodd" class="icon-gallery__path--3" d="M28.998,31.038 L20.937,28.879 L2.514,23.942 L14.623,17.941 L19.245,25.946 L16.934,21.943 L24.753,16.942 L29.964,25.127 L30.369,25.920 L28.998,31.038 Z"/>
            <path fill-rule="evenodd" class="icon-gallery__path--4" d="M15.457,8.699 C16.909,9.088 17.772,10.581 17.382,12.034 C16.993,13.487 15.500,14.349 14.047,13.960 C12.594,13.571 11.732,12.078 12.121,10.625 C12.511,9.172 14.004,8.310 15.457,8.699 Z"/></svg>',
        'mod_contact' => '<svg id="icon-contact" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43px" height="35px">
            <path fill-rule="evenodd" class="icon-contact__path--1" d="M41.993,9.503 L21.822,15.766 L7.486,0.257 C7.014,0.502 6.642,0.932 6.495,1.480 L0.761,22.880 C0.614,23.431 0.722,23.991 1.011,24.441 L21.175,18.182 L35.332,33.641 L36.506,32.454 L42.239,11.058 C42.387,10.510 42.279,9.951 41.993,9.503 Z"/>
            <path fill-rule="evenodd" class="icon-contact__path--2" d="M2.161,25.283 L35.851,34.311 L27.193,18.495 L15.807,15.444 L1.011,24.441 C1.268,24.840 1.667,25.151 2.161,25.283 Z"/>
            <path fill-rule="evenodd" class="icon-contact__path--3" d="M8.909,0.099 C8.412,-0.034 7.909,0.037 7.486,0.257 L18.975,21.133 C19.497,22.080 20.680,22.397 21.605,21.838 L41.993,9.503 C41.737,9.101 41.336,8.787 40.840,8.655 L8.909,0.099 L8.909,0.099 Z"/></svg>',
        'mod_forum' => '<svg id="icon-forum" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="35px" height="36px">
            <path fill-rule="evenodd" class="icon-forum__path--1" d="M15.826,9.458 L31.816,13.743 C33.787,14.271 34.963,16.308 34.435,18.279 L31.718,28.419 C31.189,30.390 29.153,31.567 27.182,31.038 L27.258,30.755 L26.015,35.392 L21.523,29.522 L11.191,26.754 C9.220,26.226 8.044,24.189 8.573,22.218 L11.290,12.077 C11.818,10.106 13.855,8.930 15.826,9.458 Z"/>
            <path fill-rule="evenodd" class="icon-forum__path--2" d="M23.974,4.394 L7.984,0.110 C6.013,-0.418 3.976,0.758 3.448,2.729 L0.731,12.870 C0.203,14.841 1.379,16.878 3.350,17.406 L3.426,17.122 L2.183,21.759 L9.008,18.922 L19.340,21.690 C21.311,22.218 23.348,21.042 23.876,19.071 L26.593,8.931 C27.122,6.960 25.946,4.923 23.974,4.394 Z"/>
            <path fill-rule="evenodd" class="icon-forum__path--3" d="M20.726,12.722 C20.441,13.788 19.345,14.421 18.278,14.136 C17.212,13.850 16.580,12.754 16.865,11.688 C17.151,10.622 18.247,9.989 19.313,10.275 C20.379,10.560 21.012,11.656 20.726,12.722 ZM13.449,12.841 C12.383,12.556 11.750,11.460 12.036,10.394 C12.321,9.327 13.417,8.695 14.483,8.980 C15.550,9.266 16.182,10.362 15.897,11.428 C15.611,12.494 14.515,13.127 13.449,12.841 ZM8.619,11.547 C7.553,11.262 6.920,10.166 7.206,9.100 C7.492,8.033 8.588,7.401 9.654,7.686 C10.720,7.972 11.353,9.068 11.067,10.134 C10.781,11.200 9.685,11.833 8.619,11.547 Z"/></svg>',
        'mod_login' => '<svg id="icon-login" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="36px">
            <path fill-rule="evenodd" class="icon-login__path--1" d="M29.409,21.225 L26.234,33.071 C25.682,35.133 23.562,36.357 21.500,35.805 L12.644,33.432 L3.789,31.059 C1.727,30.507 0.503,28.387 1.055,26.325 L4.229,14.478 C4.782,12.416 6.902,11.192 8.964,11.745 L17.820,14.117 L26.675,16.490 C28.737,17.043 29.961,19.163 29.409,21.225 Z"/>
            <path fill-rule="evenodd" class="icon-login__path--2" d="M14.313,27.204 L14.313,27.204 C13.324,26.939 12.731,25.913 12.996,24.924 L13.960,21.327 C14.225,20.337 15.252,19.745 16.241,20.010 L16.241,20.010 C17.230,20.275 17.822,21.301 17.557,22.290 L16.593,25.887 C16.328,26.877 15.303,27.469 14.313,27.204 Z"/>
            <path fill-rule="evenodd" class="icon-login__path--3" d="M21.319,1.059 C15.699,-0.447 10.010,2.497 8.637,7.620 L7.593,11.517 C7.571,11.600 7.557,11.683 7.553,11.765 C8.003,11.721 8.467,11.751 8.929,11.875 L9.991,12.160 L11.035,8.263 C12.054,4.462 16.379,2.306 20.676,3.457 C24.973,4.608 27.641,8.638 26.622,12.439 L25.578,16.336 L26.640,16.621 C27.102,16.745 27.519,16.951 27.887,17.214 C27.924,17.140 27.954,17.062 27.976,16.979 L29.020,13.082 C30.393,7.958 26.938,2.565 21.319,1.059 Z"/></svg>',
        'mod_timetable' => '<svg id="icon-timetable" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="38px">
            <path fill-rule="evenodd" class="icon-timetable__path--1" d="M8.003,1.685 L0.490,29.721 L28.493,37.224 L36.005,9.188 L8.003,1.685 Z"/>
            <path fill-rule="evenodd" class="icon-timetable__path--2" d="M8.003,1.685 L6.184,8.471 L34.187,15.974 L36.005,9.188 L8.003,1.685 Z"/>
            <path fill-rule="evenodd" class="icon-timetable__path--3" d="M28.254,22.666 L28.254,22.666 L27.995,23.632 L27.995,23.632 L26.960,27.496 L26.960,27.496 L26.701,28.462 L26.701,28.462 L25.407,33.292 L15.748,30.703 L15.748,30.703 L15.265,30.574 L10.918,29.409 L10.918,29.409 L10.139,29.201 L5.123,27.856 L6.417,23.027 L6.417,23.027 L6.675,22.061 L6.675,22.061 L9.264,12.401 L29.548,17.837 L28.254,22.666 ZM27.029,23.373 L23.166,22.338 L22.130,26.202 L25.994,27.237 L27.029,23.373 ZM17.301,24.908 L21.164,25.943 L22.200,22.079 L18.336,21.044 L17.301,24.908 ZM17.370,20.785 L13.506,19.750 L12.471,23.614 L16.335,24.649 L17.370,20.785 ZM24.700,32.067 L25.735,28.203 L21.871,27.168 L20.836,31.032 L19.870,30.773 L20.905,26.909 L17.042,25.874 L16.007,29.738 L24.700,32.067 ZM15.041,29.479 L16.076,25.615 L12.212,24.580 L11.177,28.443 L15.041,29.479 ZM6.347,27.149 L10.211,28.185 L11.246,24.321 L7.383,23.286 L6.347,27.149 ZM11.505,23.355 L12.540,19.491 L8.677,18.456 L7.641,22.320 L11.505,23.355 ZM9.971,13.626 L8.935,17.490 L12.799,18.525 L13.834,14.662 L9.971,13.626 ZM14.800,14.920 L13.765,18.784 L17.629,19.819 L18.664,15.956 L14.800,14.920 ZM19.630,16.215 L18.595,20.078 L22.458,21.114 L23.494,17.250 L19.630,16.215 ZM23.424,21.372 L27.288,22.408 L28.323,18.544 L24.460,17.509 L23.424,21.372 Z"/>
            <path fill-rule="evenodd" class="icon-timetable__path--4" d="M31.360,11.075 L26.530,9.781 L27.824,4.952 L32.654,6.246 L31.360,11.075 ZM11.075,5.640 L12.369,0.810 L17.199,2.104 L15.905,6.934 L11.075,5.640 Z"/>
            <path fill-rule="evenodd" class="icon-timetable__path--5" d="M8.677,18.456 L12.540,19.491 L11.505,23.355 L7.641,22.320 L8.677,18.456 Z"/></svg>',
        'mod_video' => '<svg id="icon-video" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38px" height="34px">
            <path fill-rule="evenodd" class="icon-video__path--1" d="M31.215,5.833 L13.999,1.220 C9.683,0.063 5.235,2.667 4.065,7.034 L1.083,18.163 C-0.088,22.531 2.463,27.009 6.779,28.165 L23.995,32.778 C28.311,33.935 32.759,31.332 33.929,26.964 L36.911,15.836 C38.081,11.468 35.531,6.989 31.215,5.833 Z"/>
            <path fill-rule="evenodd" class="icon-video__path--2" d="M24.325,19.573 L12.831,21.418 C12.522,21.477 12.683,21.303 12.765,20.996 L15.510,10.753 C15.593,10.441 15.549,10.213 15.786,10.427 L24.606,18.446 C24.870,18.685 24.674,19.507 24.325,19.573 Z"/></svg>',
        'mod_jokes' => '<svg id="icon-jokes" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="36px">
            <path fill-rule="evenodd" class="icon-jokes__path--1" d="M22.997,1.549 C13.645,-0.957 4.032,4.593 1.527,13.945 C-0.979,23.297 4.571,32.910 13.923,35.415 C23.274,37.921 32.887,32.372 35.393,23.019 C37.899,13.667 32.349,4.055 22.997,1.549 Z"/>
            <path fill-rule="evenodd" class="icon-jokes__path--2" d="M15.554,29.326 C11.147,28.145 7.939,24.721 6.826,20.611 C6.735,20.276 6.830,19.919 7.075,19.673 C7.320,19.427 7.677,19.333 8.013,19.423 L27.037,24.520 C27.371,24.610 27.633,24.872 27.722,25.206 C27.814,25.541 27.718,25.899 27.471,26.143 C24.452,29.145 19.962,30.507 15.554,29.326 Z"/>
            <path fill-rule="evenodd" class="icon-jokes__path--3" d="M25.424,17.767 C25.137,18.838 24.037,19.473 22.966,19.186 C21.896,18.899 21.260,17.799 21.547,16.728 C21.834,15.657 22.935,15.022 24.005,15.309 C25.076,15.596 25.711,16.696 25.424,17.767 ZM14.273,16.857 C13.202,16.570 12.567,15.469 12.854,14.399 C13.141,13.328 14.241,12.693 15.312,12.980 C16.382,13.266 17.018,14.367 16.731,15.438 C16.444,16.508 15.344,17.144 14.273,16.857 Z"/></svg>',
    );
?>
</head>
<body>
    <div class="page" tabindex="-1">
        <ul class="skip-links list-unstyled">
            <li><a href="#main-menu"><?php echo __('skip to main menu')?></a></li> 
            <li><a href="#add-menu"><?php echo __('skip to additional menu')?></a></li> 
            <li><a href="#link-content"><?php echo __('skip to content')?></a></li>
            <li><a href="#search"><?php echo __('skip to search')?></a></li>
            <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
        </ul>