<?php
$outRowTopModulesCount = count($outRowTopModules);
if ($outRowTopModulesCount > 0): ?>
<div class="modules-top">
    <div class="container">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-top__object" width="119px" height="138px">
            <path fill-rule="evenodd" d="M114.955,89.829 C110.512,101.279 99.967,114.903 87.528,124.563 C81.308,129.392 74.614,133.228 68.095,135.443 C64.835,136.550 61.620,137.252 58.521,137.461 C56.972,137.565 55.452,137.546 53.969,137.392 C53.228,137.314 52.496,137.204 51.774,137.057 C51.053,136.911 50.342,136.730 49.643,136.512 C43.366,134.538 40.809,127.764 35.206,125.544 C31.164,123.927 26.070,125.449 22.072,124.044 C9.389,119.420 2.570,102.144 1.098,82.748 C0.300,72.982 0.412,62.207 2.296,52.778 C2.776,50.431 3.378,48.182 4.129,46.079 C4.879,43.977 5.778,42.022 6.847,40.262 C8.979,36.736 11.810,34.005 15.376,32.286 C24.290,28.207 41.325,33.284 53.324,37.319 C65.862,41.959 73.363,45.556 75.679,52.480 C85.665,37.819 71.636,44.035 65.086,27.055 C61.876,17.603 61.187,11.223 66.580,0.204 C79.990,16.175 86.013,20.521 85.025,37.873 C84.770,42.631 81.705,48.323 79.436,54.261 C91.792,50.151 101.934,47.420 109.736,53.072 C113.554,55.806 116.627,60.458 117.879,66.714 C118.502,69.838 118.657,73.350 118.209,77.209 C117.985,79.139 117.610,81.156 117.072,83.259 C116.803,84.311 116.493,85.384 116.141,86.479 C115.788,87.574 115.394,88.690 114.955,89.829 L114.955,89.829 ZM116.545,72.729 C116.828,61.389 109.769,54.104 101.426,52.743 C92.821,51.089 82.931,55.360 71.505,60.362 C70.464,50.443 62.733,42.670 41.065,38.274 C19.835,33.185 8.557,46.491 6.359,65.631 C3.603,84.145 5.660,103.715 12.317,112.516 C14.460,115.352 19.678,118.982 22.316,120.117 C26.626,121.971 32.524,120.400 36.430,121.892 C41.035,123.635 43.437,129.262 48.119,130.996 C61.525,135.913 78.615,129.193 92.152,117.086 C98.925,111.037 104.832,103.667 109.154,95.963 C113.479,88.263 116.189,80.197 116.545,72.729 L116.545,72.729 Z"/>
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-top__object--2" width="62px" height="59px">
            <path fill-rule="evenodd" d="M54.688,26.610 C51.314,28.905 47.935,30.883 46.586,32.829 C50.515,41.563 56.123,49.915 62.000,58.409 C49.888,53.567 43.272,47.084 36.182,41.907 C30.632,44.180 25.046,49.212 18.791,52.708 C20.421,46.963 24.113,39.068 25.118,34.046 C19.977,28.914 9.867,27.669 0.646,26.052 C8.561,21.868 19.026,20.958 24.643,18.581 C20.906,13.237 21.991,7.683 19.400,0.500 C26.191,5.233 32.328,11.646 37.587,18.465 C45.114,19.451 50.790,19.300 60.601,19.610 C61.367,21.735 58.023,24.335 54.688,26.610 ZM49.719,22.941 C44.593,22.150 40.928,22.202 35.985,21.428 C32.461,16.743 28.500,12.239 23.442,9.043 C25.628,13.688 24.614,17.477 27.103,21.274 C23.071,22.832 15.897,23.102 10.830,25.611 C17.044,27.165 23.651,28.509 27.363,32.168 C26.776,35.715 24.629,41.219 24.110,45.070 C28.336,42.534 31.669,39.138 35.453,37.428 C40.758,40.560 45.078,44.690 50.898,48.561 C48.455,42.291 45.153,36.704 42.216,31.043 C43.887,28.340 50.640,25.834 49.719,22.941 Z"/>
        </svg>
        <div class="row">
            <?php
                $i = 0;
                $className = ['module-1','module-2','module-3', 'module-4'];
                foreach ($outRowTopModules as $module):
                    $i++;
                    $mod = 'module' . $i;
                ?>
                <div id="<?php echo $module['mod_name']; ?>" class="module <?php echo $className[$outRowTopModulesCount - 1] ?>">
                    <svg class="vector-decoration bottom" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                        <path fill-rule="evenodd" fill="#000" opacity="0.1" transform="translate(-2,3)" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                        <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    </svg>
                    <svg class="vector-decoration top" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
                        <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
                    </svg>
                    <div class="modules-top__holder">
                        <div class="modules-top__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="79px" height="79px" class="modules-top__icon--background">
                                <defs>
                                    <filter id="<?php echo 'module-top-icon-' . $i; ?>">
                                        <feOffset in="SourceAlpha" dx="0" dy="0" />
                                        <feGaussianBlur result="blurOut" stdDeviation="3.162" />
                                        <feFlood flood-color="rgb(1, 1, 1)" result="floodOut" />
                                        <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                        <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.15"/></feComponentTransfer>
                                        <feBlend mode="normal" in2="SourceGraphic" />
                                    </filter>
                                </defs>
                                <g filter="<?php echo 'url(#module-top-icon-' . $i . ')'; ?>">
                                    <path fill-rule="evenodd" d="M39.516,73.074 C33.816,73.074 28.451,71.648 23.749,69.141 L6.000,73.000 L9.863,55.229 C7.373,50.541 5.957,45.194 5.957,39.516 C5.957,20.982 20.982,5.957 39.516,5.957 C58.050,5.957 73.074,20.982 73.074,39.516 C73.074,58.049 58.050,73.074 39.516,73.074 Z"/>
                                </g>
                            </svg>
                            <?php if(array_key_exists($module['mod_name'], $icons)): echo $icons[$module['mod_name']]; endif; ?>
                        </div>
                        <h2 class="modules-top__name name-<?php echo $i; ?>"><?php echo $module['name']; ?></h2>
                        <div class="modules-top__content"><?php echo get_module($module['mod_name'], 'top'); ?></div>
                    </div>
                </div>
            <?php
                endforeach;
            ?>
        </div>
    </div>
</div>
<?php endif;
