<?php
if ($pagination['end'] > 1)
{
    ?>
<div class="row">
    <div class="pagination-wrapper col-xs-12">
        <?php
        $active_page = $pagination['active'];

        if ( !isset($pagination['active']) )
        {
            $active_page = 1;
        }
        ?>
        <p><?php echo __('page')?>: <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
        <ul class="list-unstyled list-inline text-center">
        <?php
        if ($pagination['start'] != $pagination['prev'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btn-first">
                    <span class="sr-only"><?php echo __('first page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17px" height="18px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-first" x="0px" y="0px" width="17px" height="18px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(32, 61, 76)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-first)">
                            <path fill-rule="evenodd" d="M8.883,16.244 L5.995,16.244 L14.116,8.124 L5.995,0.002 L8.883,0.002 L17.005,8.124 L8.883,16.244 ZM2.883,16.244 L-0.005,16.244 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.244 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btn-prev">
                    <span class="sr-only"><?php echo __('prev page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11px" height="18px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-prev" x="0px" y="0px" width="11px" height="18px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(32, 61, 76)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-prev)">
                            <path fill-rule="evenodd" d="M-0.005,16.244 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.244 L-0.005,16.244 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <?php
        }		
        foreach ($pagination as $k => $v)
        {
            if (is_numeric($k))
            {
                ?>
            <li>
                <a href="<?php echo $url . $v?>" rel="nofollow" class="btn-page">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <?php echo $v?>
                </a>
            </li>
                <?php
            } else if ($k == 'active')
            {
                ?>
            <li>
                <span class="page-active">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <?php echo $v?>
                </span>
            </li>
                <?php
            }			
        }		
        if ($pagination['active'] != $pagination['end'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btn-next">
                    <span class="sr-only"><?php echo __('next page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11px" height="18px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-next" x="0px" y="0px" width="11px" height="18px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(32, 61, 76)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-next)">
                            <path fill-rule="evenodd" d="M-0.005,16.244 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.244 L-0.005,16.244 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btn-last">
                    <span class="sr-only"><?php echo __('last page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17px" height="18px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-last" x="0px" y="0px" width="17px" height="18px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(32, 61, 76)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-last)">
                            <path fill-rule="evenodd" d="M8.883,16.244 L5.995,16.244 L14.116,8.124 L5.995,0.002 L8.883,0.002 L17.005,8.124 L8.883,16.244 ZM2.883,16.244 L-0.005,16.244 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.244 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <?php
        }
        ?>	
        </ul>
    </div>
</div>
    <?php
}
?>