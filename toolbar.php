<a id="top"></a>
<nav id="toolbar" class="toolbar">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="index.php" class="toolbar__home">
                    <span class="sr-only"><?php echo __("home page"); ?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 17 15">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="toolbar-home-filter" x="0px" y="0px" width="17px" height="15px"  >
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="1" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#toolbar-home-filter)">
                            <path fill-rule="evenodd" d="M4.014,6.962 L4.014,11.975 L7.013,11.975 L7.013,8.414 C7.013,8.164 7.221,7.961 7.479,7.961 L9.546,7.961 C9.803,7.961 10.012,8.164 10.012,8.414 L10.012,11.975 L12.984,11.975 L13.010,6.962 L14.994,6.962 L8.511,0.939 L1.968,6.962 L4.014,6.962 Z"/>
                        </g>
                    </svg>
                </a>
                <?php
                    $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                    $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                ?>
                <h1><?php echo $pageInfo['name']; ?></h1>
            </div>
            <div class="col-lg-7 col-md-8 col-lg-offset-1">
                <div class="toolbar__address">
                    <?php
                        echo $headerAddress;
                        if ($pageInfo['email'] != '') {
                            echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                        }
                    ?>
                </div>
                <ul class="toolbar__links">
                    <li class="bip">
                        <a href="<?php echo $pageInfo['bip'] ?>">
                            <img src="<?php echo $pathTemplate; ?>/images/toolbar/bip.png" alt="<?php echo __('bip'); ?>">
                        </a>
                    </li>
                    <li class="fonts">
                        <span><?php echo __('font size'); ?></span>
                        <ul>
                            <li>
                                <a href="ch_style.php?style=0">
                                    <span class="sr-only"><?php echo __('font default')?></span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16px" height="16px">
                                        <defs>
                                            <filter filterUnits="userSpaceOnUse" id="font-default-filter" x="0px" y="0px" width="16px" height="16px">
                                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                                <feFlood flood-color="rgb(182, 224, 246)" result="floodOut" />
                                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                                <feMerge>
                                                    <feMergeNode/>
                                                    <feMergeNode in="SourceGraphic"/>
                                                </feMerge>
                                            </filter>
                                        </defs>
                                        <g filter="url(#font-default-filter)">
                                            <path fill-rule="evenodd" d="M9.219,10.303 L9.922,12.305 C9.961,12.415 10.000,12.545 10.039,12.695 C10.078,12.845 10.098,13.001 10.098,13.164 C10.098,13.301 10.072,13.418 10.019,13.516 C9.967,13.613 9.901,13.693 9.819,13.755 C9.738,13.817 9.645,13.862 9.541,13.892 C9.437,13.921 9.336,13.935 9.238,13.935 L8.457,13.935 L8.457,15.000 L15.059,15.000 L15.059,13.935 L14.697,13.935 C14.541,13.935 14.391,13.913 14.248,13.867 C14.105,13.822 13.970,13.745 13.843,13.638 C13.716,13.530 13.599,13.384 13.491,13.198 C13.384,13.013 13.281,12.780 13.184,12.500 L9.004,0.722 L6.260,0.722 L1.924,12.480 C1.807,12.780 1.691,13.024 1.577,13.213 C1.463,13.402 1.343,13.550 1.216,13.657 C1.089,13.765 0.952,13.838 0.806,13.877 C0.659,13.916 0.498,13.935 0.322,13.935 L0.078,13.935 L0.078,15.000 L5.283,15.000 L5.283,13.935 L4.443,13.935 C4.326,13.935 4.214,13.919 4.106,13.887 C3.999,13.854 3.903,13.807 3.818,13.745 C3.734,13.683 3.665,13.603 3.613,13.506 C3.561,13.408 3.535,13.294 3.535,13.164 C3.535,13.001 3.563,12.811 3.618,12.593 C3.673,12.375 3.727,12.184 3.779,12.021 L4.355,10.303 L9.219,10.303 ZM8.818,9.082 L4.775,9.082 L5.957,5.703 C6.035,5.469 6.118,5.224 6.206,4.971 C6.294,4.717 6.382,4.463 6.470,4.209 C6.558,3.955 6.641,3.708 6.719,3.467 C6.797,3.226 6.865,3.005 6.924,2.803 C6.976,3.037 7.036,3.283 7.104,3.540 C7.173,3.797 7.244,4.056 7.319,4.316 C7.394,4.577 7.469,4.832 7.544,5.083 C7.619,5.334 7.692,5.573 7.764,5.801 L8.818,9.082 Z"/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="ch_style.php?style=r1">
                                    <span class="sr-only"><?php echo __('font bigger')?></span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19px" height="18px">
                                        <defs>
                                            <filter filterUnits="userSpaceOnUse" id="font-bigger-filter" x="0px" y="0px" width="19px" height="18px"  >
                                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                                <feFlood flood-color="rgb(182, 224, 246)" result="floodOut" />
                                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                                <feMerge>
                                                    <feMergeNode/>
                                                    <feMergeNode in="SourceGraphic"/>
                                                </feMerge>
                                            </filter>
                                        </defs>
                                        <g filter="url(#font-bigger-filter)">
                                            <path fill-rule="evenodd" d="M17.000,3.000 L17.000,5.000 L16.000,5.000 L16.000,3.000 L14.000,3.000 L14.000,2.000 L16.000,2.000 L16.000,-0.000 L17.000,-0.000 L17.000,2.000 L19.000,2.000 L19.000,3.000 L17.000,3.000 ZM15.515,14.928 C15.638,15.141 15.773,15.310 15.919,15.433 C16.065,15.557 16.220,15.645 16.385,15.697 C16.550,15.750 16.722,15.776 16.902,15.776 L17.317,15.776 L17.317,17.000 L9.725,17.000 L9.725,15.776 L10.624,15.776 C10.736,15.776 10.852,15.759 10.972,15.725 C11.092,15.692 11.198,15.639 11.292,15.568 C11.386,15.497 11.462,15.405 11.522,15.293 C11.582,15.181 11.612,15.046 11.612,14.889 C11.612,14.702 11.590,14.522 11.545,14.349 C11.500,14.177 11.455,14.028 11.410,13.900 L10.602,11.598 L5.009,11.598 L4.346,13.575 C4.286,13.762 4.224,13.981 4.161,14.232 C4.097,14.482 4.065,14.702 4.065,14.889 C4.065,15.038 4.095,15.169 4.155,15.282 C4.215,15.394 4.294,15.486 4.391,15.557 C4.488,15.628 4.599,15.682 4.722,15.720 C4.846,15.757 4.975,15.776 5.110,15.776 L6.076,15.776 L6.076,17.000 L0.090,17.000 L0.090,15.776 L0.371,15.776 C0.573,15.776 0.758,15.753 0.926,15.708 C1.095,15.663 1.252,15.579 1.398,15.456 C1.544,15.332 1.683,15.162 1.814,14.945 C1.945,14.728 2.078,14.447 2.212,14.102 L7.199,0.581 L10.354,0.581 L15.161,14.125 C15.273,14.447 15.391,14.715 15.515,14.928 ZM8.928,6.421 C8.846,6.159 8.761,5.884 8.676,5.595 C8.589,5.307 8.503,5.013 8.417,4.714 C8.331,4.414 8.249,4.117 8.170,3.821 C8.091,3.525 8.022,3.243 7.962,2.973 C7.895,3.205 7.816,3.460 7.727,3.737 C7.637,4.014 7.541,4.298 7.440,4.590 C7.339,4.882 7.238,5.174 7.137,5.466 C7.036,5.758 6.940,6.039 6.851,6.309 L5.492,10.194 L10.141,10.194 L8.928,6.421 Z"/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="ch_style.php?style=r2">
                                    <span class="sr-only"><?php echo __('font biggest')?></span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26px" height="20px">
                                        <defs>
                                            <filter filterUnits="userSpaceOnUse" id="font-biggest-filter" x="0px" y="0px" width="26px" height="20px"  >
                                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                                <feFlood flood-color="rgb(182, 224, 246)" result="floodOut" />
                                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                                <feMerge>
                                                    <feMergeNode/>
                                                    <feMergeNode in="SourceGraphic"/>
                                                </feMerge>
                                            </filter>
                                        </defs>
                                        <g filter="url(#font-biggest-filter)">
                                            <path fill-rule="evenodd" d="M24.000,4.000 L24.000,6.000 L23.000,6.000 L23.000,4.000 L21.000,4.000 L21.000,3.000 L23.000,3.000 L23.000,1.000 L24.000,1.000 L24.000,3.000 L26.000,3.000 L26.000,4.000 L24.000,4.000 ZM18.000,6.000 L17.000,6.000 L17.000,4.000 L15.000,4.000 L15.000,3.000 L17.000,3.000 L17.000,1.000 L18.000,1.000 L18.000,3.000 L20.000,3.000 L20.000,4.000 L18.000,4.000 L18.000,6.000 ZM17.538,16.658 C17.678,16.899 17.831,17.089 17.996,17.229 C18.161,17.369 18.336,17.468 18.522,17.527 C18.708,17.587 18.903,17.616 19.106,17.616 L19.576,17.616 L19.576,19.000 L10.994,19.000 L10.994,17.616 L12.010,17.616 C12.137,17.616 12.268,17.597 12.403,17.559 C12.539,17.521 12.659,17.462 12.765,17.381 C12.871,17.301 12.958,17.197 13.025,17.070 C13.093,16.943 13.127,16.791 13.127,16.613 C13.127,16.402 13.102,16.199 13.051,16.004 C13.000,15.809 12.949,15.640 12.898,15.496 L11.984,12.893 L5.662,12.893 L4.913,15.128 C4.845,15.339 4.775,15.587 4.704,15.871 C4.632,16.154 4.596,16.402 4.596,16.613 C4.596,16.782 4.629,16.931 4.697,17.057 C4.765,17.184 4.854,17.288 4.964,17.369 C5.074,17.449 5.199,17.510 5.338,17.553 C5.478,17.595 5.624,17.616 5.776,17.616 L6.868,17.616 L6.868,19.000 L0.102,19.000 L0.102,17.616 L0.419,17.616 C0.647,17.616 0.857,17.591 1.047,17.540 C1.238,17.489 1.415,17.394 1.580,17.254 C1.746,17.115 1.902,16.922 2.050,16.677 C2.198,16.431 2.349,16.114 2.501,15.725 L8.138,0.439 L11.705,0.439 L17.139,15.750 C17.266,16.114 17.399,16.416 17.538,16.658 ZM10.093,7.041 C10.000,6.745 9.904,6.434 9.807,6.108 C9.710,5.782 9.612,5.450 9.515,5.111 C9.418,4.773 9.325,4.436 9.236,4.102 C9.147,3.768 9.069,3.448 9.001,3.143 C8.925,3.406 8.836,3.694 8.734,4.007 C8.633,4.320 8.525,4.642 8.411,4.972 C8.296,5.302 8.182,5.632 8.068,5.962 C7.954,6.292 7.846,6.609 7.744,6.914 L6.208,11.306 L11.464,11.306 L10.093,7.041 Z"/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                        if ($_SESSION['contr'] == 0) {
                            $set_contrast = 1;
                            $contrast_txt = __('contrast version');
                        } else {
                            $set_contrast = 0;
                            $contrast_txt = __('graphic version');
                        }
                    ?>
                    <li class="contrast">
                        <span><?php echo __('font contrast'); ?></span>
                        <a href="ch_style.php?contr=<?php echo $set_contrast ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23px" height="23px">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="contrast-filter" x="0px" y="0px" width="23px" height="23px">
                                        <feOffset in="SourceAlpha" dx="0" dy="1" />
                                        <feGaussianBlur result="blurOut" stdDeviation="0" />
                                        <feFlood flood-color="rgb(182, 224, 246)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                </defs>
                                <g filter="url(#contrast-filter)">
                                    <path fill-rule="evenodd" d="M21.865,10.120 C21.666,10.120 20.987,10.120 20.788,10.120 C20.180,10.120 19.687,10.514 19.687,11.000 C19.687,11.486 20.180,11.880 20.788,11.880 C20.987,11.880 21.666,11.880 21.865,11.880 C22.473,11.880 22.965,11.486 22.965,11.000 C22.965,10.514 22.473,10.120 21.865,10.120 ZM11.965,4.950 C8.610,4.950 5.915,7.645 5.915,11.000 C5.915,14.355 8.610,17.050 11.965,17.050 C15.320,17.050 18.015,14.355 18.015,11.000 C18.015,7.645 15.321,4.950 11.965,4.950 ZM12.075,15.400 C9.534,15.400 7.565,13.431 7.565,11.000 C7.565,8.569 9.534,6.600 12.075,6.600 L12.075,15.400 ZM4.243,11.000 C4.243,10.514 3.773,10.120 3.165,10.120 C2.963,10.120 2.268,10.120 2.066,10.120 C1.458,10.120 0.966,10.514 0.966,11.000 C0.966,11.486 1.458,11.880 2.066,11.880 C2.268,11.880 2.963,11.880 3.165,11.880 C3.773,11.880 4.243,11.486 4.243,11.000 ZM11.965,3.300 C12.451,3.300 12.846,2.808 12.846,2.200 C12.846,1.998 12.846,1.301 12.846,1.100 C12.846,0.492 12.451,-0.000 11.965,-0.000 C11.479,-0.000 11.085,0.492 11.085,1.100 C11.085,1.301 11.085,1.998 11.085,2.200 C11.085,2.808 11.480,3.300 11.965,3.300 ZM11.965,18.700 C11.479,18.700 11.085,19.192 11.085,19.800 C11.085,20.001 11.085,20.698 11.085,20.900 C11.085,21.507 11.479,22.000 11.965,22.000 C12.451,22.000 12.846,21.507 12.846,20.900 C12.846,20.698 12.846,20.001 12.846,19.800 C12.846,19.192 12.451,18.700 11.965,18.700 ZM20.067,4.142 C20.497,3.713 20.567,3.086 20.223,2.742 C19.879,2.399 19.253,2.468 18.823,2.898 C18.705,3.016 18.172,3.549 18.054,3.668 C17.624,4.097 17.554,4.724 17.898,5.068 C18.241,5.411 18.868,5.342 19.298,4.912 C19.416,4.794 19.950,4.261 20.067,4.142 ZM4.633,17.088 C4.514,17.207 3.981,17.740 3.863,17.858 C3.434,18.287 3.364,18.914 3.707,19.258 C4.051,19.601 4.678,19.532 5.107,19.102 C5.226,18.984 5.759,18.451 5.877,18.332 C6.307,17.903 6.377,17.276 6.033,16.932 C5.689,16.589 5.062,16.659 4.633,17.088 ZM5.108,2.898 C4.678,2.468 4.052,2.398 3.708,2.742 C3.364,3.086 3.434,3.713 3.863,4.142 C3.982,4.260 4.515,4.794 4.633,4.912 C5.063,5.342 5.689,5.411 6.033,5.068 C6.377,4.724 6.307,4.097 5.878,3.668 C5.760,3.549 5.227,3.016 5.108,2.898 ZM18.054,18.332 C18.172,18.451 18.705,18.984 18.824,19.102 C19.253,19.532 19.880,19.602 20.223,19.258 C20.567,18.914 20.497,18.287 20.068,17.858 C19.950,17.740 19.417,17.207 19.298,17.088 C18.869,16.659 18.241,16.589 17.898,16.932 C17.554,17.277 17.624,17.903 18.054,18.332 Z"/>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li id="searchbar" class="search">
                        <a id="search" tabindex="-1" style="display: inline;"></a>
                        <h2 class="sr-only"><?php echo __('search')?></h2>
                        <form id="searchForm" name="f_szukaj" method="get" action="index.php">
                            <input name="c" type="hidden" value="search" />
                            <label for="kword"><span class="sr-only"><?php echo __('search query')?></span></label>
                            <input type="text" id="kword" name="kword" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
                            <button type="submit" name="search" id="toolbar-search"><?php echo __('search action'); ?></button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<a id="main-menu" tabindex="-1" style="display: inline;"></a>
<nav id="menu-top" class="menu-top">
    <p class="sr-only"><?php echo __('main menu'); ?></p>
    <div class="navbar-header">
        <button class="navbar-toggle collapsed" aria-controls="navbar-top" aria-expanded="false" data-target="#navbar-top" id="navbar-mobile" data-toggle="collapse" type="button">
            <i class="icon" aria-hidden="true"></i>
            <i class="icon" aria-hidden="true"></i>
            <i class="icon" aria-hidden="true"></i>
            <span class="sr-only"><?php echo __('expand')?></span>
            <span class="title sr-only"><?php echo __('main menu')?></span>
        </button>
    </div>
    <div id="navbar-top" class="navbar-collapse menu">
        <svg class="vector-decoration" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 50.39">
        <path fill-rule="evenodd" fill="#000" opacity="0.1" transform="translate(0,3)" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
            <path fill-rule="evenodd" d="M70,50V0H0C70.52,0,70,55.52,70,50Z"/>
        </svg>
        <?php get_menu_tree('tm', 0, 0, '', false, '', false, true, true, false, true, true, '', true); ?>    
    </div>
</nav>
