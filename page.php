<div class="main-text">
<h2 class="main-header"><span><?php echo $pageName?></span></h2>
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
	
if ($showPage)
{
    echo $row['text'];
		
    if (! check_html_text($row['author'], '') )
    {
	?>
	<div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
	<?php
    }
			
    /*
     * Articles
     */
    if ($numArticles > 0)
    {	
	$i = 0;
	?>		
	<div class="article-wrapper">
	    <?php
	    foreach ($outRowArticles as $row)
            {
                $i++;
		$highlight = $url = $target = $url_title = $protect = '';
			
		if ($row['protected'] == 1)
		{
        $protect = '<span class="protectedPage"></span>';
        $url_title = ' title="' . __('page requires login') . '"';
		}				
				
		if (trim($row['ext_url']) != '')
		{
		    if ($row['new_window'] == '1')
		    {
			$target = ' target="_blank"';
		    }	
		    $url_title = ' title="' . __('opens in new window') . '"';
		    $url = ref_replace($row['ext_url']);					
		} else
		{
		    if ($row['url_name'] != '')
		    {
			$url = 'art,' . $row['id_art'] . ',' . $row['url_name'];
		    } else
		    {
			$url = 'index.php?c=article&amp;id=' . $row['id_art'];
		    }
		}	
				
		$margin = ' no-photo';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $margin = '';
		}				
				
		$row['show_date'] = substr($row['show_date'], 0, 10);
		
		$highlight = '';
		if ($row['highlight'] == 1)
		{
		    $highlight = ' highlight-article';
		}			
		?>
        <div class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>">
            <div class="lead-text<?php echo $margin; ?>">
                <h4 class="article-title <?php echo $margin?>">
                    <a href="<?php echo $url?>" <?php echo $url_title . $target ?>>
                        <span><?php echo $row['name'] . $protect?></span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11px" height="17px"><path fill-rule="evenodd" d="M-0.005,16.245 L8.116,8.124 L-0.005,0.002 L2.883,0.002 L11.005,8.124 L2.883,16.245 L-0.005,16.245 Z"/></svg>
                    </a>
                </h4>
                <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                    <p class="article-date">
                        <span><?php echo $row['show_date'] ?></span>    
                    </p>
                <?php } ?>
                <div class="lead-main-text">
                    <?php echo truncate_html($row['lead_text'], 300, '...')?>
                </div>
                <div class="article-meta">
                    <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="button color-2" title="">
                        <span><?php echo __('read more') ?></span>
                        <span class="sr-only"> <?php echo __('about')?>: <?php echo $row['name']; ?></span>
                    </a>
                </div>
            </div>
            <?php
                $columnWidth = 'col-sm-12';
                if (is_array($photoLead[$row['id_art']]))
                {
                    $photo = $photoLead[$row['id_art']];
                    $columnWidth = 'col-sm-offset-5 col-sm-7';
                    ?>
                    <div class="photo-wrapper<?php echo $photoWrapper; ?>">
                        <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" class="photo fancybox" data-fancybox-group="gallery">
                            <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 370 229">
                                <defs>
                                    <pattern id="<?php echo 'gallery-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']; ?>"></image>
                                    </pattern>
                                </defs>
                                <path fill-rule="evenodd" stroke-width="2px" fill="<?php echo 'url(#gallery-image-' . $i . ')'; ?>" d="M10.000,5.000 L360.000,5.000 C362.761,5.000 365.000,7.238 365.000,10.000 L365.000,219.000 C365.000,221.761 362.761,224.000 360.000,224.000 L10.000,224.000 C7.239,224.000 5.000,221.761 5.000,219.000 L5.000,10.000 C5.000,7.238 7.239,5.000 10.000,5.000 Z"/>
                            </svg>
                        </a>
                    </div>
                    <?php
                }   
            ?>
        </div>
            <?php		
	    }
	    ?>
	    	
        <?php

	$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;id=' . $_GET['id'] . '&amp;s=';
	include (CMS_TEMPL . DS . 'pagination.php');
	?>
	</div>
	<?php
    }		
				
        /*
	 * Wypisanie plikow do pobrania
	 */
    if ($numFiles > 0)
    {	
    ?>
	<div class="files-wrapper">
            
                <h3 class="files-header"><?php echo __('files')?></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }

                    $size = file_size('download/'.$row['file']);	
                    ?>		
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>                        
                        </a>                        
                    </li>
                    <?php
                }
                ?>
                </ul>
            
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper">
        <h3 class="gallery-header"><?php echo __('gallery')?></h3>
        <ul class="gallery">
            <?php
            foreach ($outRowPhotos as $row)
            {
                $i++;
                ?>
                <li>
                    <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                        <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 370 229">
                            <defs>
                                <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']; ?>"></image>
                                </pattern>
                            </defs>
                            <path fill-rule="evenodd" stroke-width="2px" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M10.000,5.000 L360.000,5.000 C362.761,5.000 365.000,7.238 365.000,10.000 L365.000,219.000 C365.000,221.761 362.761,224.000 360.000,224.000 L10.000,224.000 C7.239,224.000 5.000,221.761 5.000,219.000 L5.000,10.000 C5.000,7.238 7.239,5.000 10.000,5.000 Z"/>
                        </svg>
                        <?php
                        if (! check_html_text($row['name'], '') ) {
                            ?>
                            <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                            <?php
                        }
                        ?>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
	</div>
	<?php
    }		

    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=page&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
}
}
?>
</div>